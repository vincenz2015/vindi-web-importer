<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://vindiweb.com
 * @since             1.0.0
 * @package           Vindi_Web_Importer
 *
 * @wordpress-plugin
 * Plugin Name:       Vindi Website Importer
 * Plugin URI:        https://vindiweb.com/importer
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            vince
 * Author URI:        https://vindiweb.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       vindi-web-importer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'VINDI_WEB_IMPORTER_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-vindi-web-importer-activator.php
 */
function activate_vindi_web_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vindi-web-importer-activator.php';
	Vindi_Web_Importer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-vindi-web-importer-deactivator.php
 */
function deactivate_vindi_web_importer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-vindi-web-importer-deactivator.php';
	Vindi_Web_Importer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_vindi_web_importer' );
register_deactivation_hook( __FILE__, 'deactivate_vindi_web_importer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-vindi-web-importer.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_vindi_web_importer() {

	$plugin = new Vindi_Web_Importer();
	$plugin->run();

}
run_vindi_web_importer();

/**
 *  @file vindi-web-importer-admin.js
 */

(function($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    /**
     * If wpis_hook is defined on this page, call it
     */
    $(window).load(function() {
        if (typeof vwi_hook === 'undefined') {
            //Do nothing
        } else {
            window[vwi_hook]($);
        }
    });

    $.extend($.expr[':'], {
        'containsi': function(elem, i, match, array)
        {
            return (elem.textContent || elem.innerText || '').toLowerCase()
            .indexOf((match[3] || "").toLowerCase()) >= 0;
        }
});
})(jQuery);

/**
 *                                  ###                     ###
 *                                  ### vwi_HOOK ROUTINES  ###
 *                                  ###                     ###
 */
 
/**
 *  @brief Import Single Page
 *  
 *  @return N/A
 *  
 *  @details Disables the submit button
 *  Invokes vwi_handle_fetch_page_html
 */
function vwi_hook_import_single_page($) {
    vwi_handle_fetch_page_html($, false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
}

/**
 *  @brief Import Multiple Pages - Invoked from Stage 2
 *  
 *  @return N/A
 *  
 *  @details Disables the submit buttons
 *           Invokes vwi_handle_fetch_page_links
 */
function vwi_hook_spider_pages($) {
    $('#submit_top').attr('disabled', 'disabled');
    $('#submit_bottom').attr('disabled', 'disabled');
    vwi_pages[vwi_url] = 'Home';
    vwi_todo_pages[vwi_url] = 'Home';
    vwi_handle_fetch_page_links($);
} 

/**
 *  @brief Import Multiple Pages - Invoked from Stage 3
 *  
 *  @return N/A
 *  
 *  @details Adds multi-select handlers for all of the check boxes
 *           
 */
function vwi_hook_multi_page_selector_control($) {
    // Handle bigger sites
    $('#vwi_form').submit(function() {
        var vwi_urls = [];
        $('[id^=vwi_url]').each(function(){
            var entry = {id:this.id,value:this.value};
            vwi_urls.push(entry);
        });
        var vwi_imports = [];
        $('[id^=vwi_import]').each(function(){
            var entry = {id:this.id,checked:this.checked};
            vwi_imports.push(entry);
        });
        var vwi_page_or_post = [];
        $('[name^=vwi_page_or_post]:checked').each(function(){
            var entry = {id:this.name,value:this.value};
            vwi_page_or_post.push(entry);
        });
        var vwi_publish = [];
        $('[id^=vwi_publish]').each(function(){
            var entry = {id:this.id,checked:this.checked};
            vwi_publish.push(entry);
        });
        var vwi_feature = [];
        $('[id^=vwi_feature]').each(function(){
            var entry = {id:this.id,checked:this.checked};
            vwi_feature.push(entry);
        });
        $('#vwi_json_urls').val(JSON.stringify(vwi_urls));
        $('#vwi_json_imports').val(JSON.stringify(vwi_imports));
        $('#vwi_json_page_or_posts').val(JSON.stringify(vwi_page_or_post));
        $('#vwi_json_publishes').val(JSON.stringify(vwi_publish));
        $('#vwi_json_features').val(JSON.stringify(vwi_feature));
        $('[id^=vwi_url]').remove();
        $('[id^=vwi_import]').remove();
        $('[name^=vwi_page_or_post]').remove();
        $('[id^=vwi_publish]').remove();
        $('[id^=vwi_feature]').remove();
        return true;
    });
    //Multi-select handler for "Import"
    var $vwi_import_chkboxes = $('.vwi_import:visible');
    var vwi_import_last_checked = null;
    $vwi_import_chkboxes.click(function(e) {
        if (!vwi_import_last_checked) {
            vwi_import_last_checked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $vwi_import_chkboxes.index(this);
            var end = $vwi_import_chkboxes.index(vwi_import_last_checked);
            $vwi_import_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_import_last_checked.checked);
            $vwi_import_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).trigger('change');
        }
        vwi_import_last_checked = this;
    });

    //Multi-select handler for "Published"
    var $vwi_publish_chkboxes = $('.vwi_publish:visible');
    var vwi_publish_last_checked = null;
    $vwi_publish_chkboxes.click(function(e) {
        if (!vwi_publish_last_checked) {
            vwi_publish_last_checked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $vwi_publish_chkboxes.index(this);
            var end = $vwi_publish_chkboxes.index(vwi_publish_last_checked);
            $vwi_publish_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_publish_last_checked.checked);
        }
        vwi_publish_last_checked = this;
    });

    //Multi-select handler for "Featured Image"
    var $vwi_feature_chkboxes = $('.vwi_feature:visible');
    var vwi_feature_last_checked = null;
    $vwi_feature_chkboxes.click(function(e) {
        if (!vwi_feature_last_checked) {
            vwi_feature_last_checked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $vwi_feature_chkboxes.index(this);
            var end = $vwi_feature_chkboxes.index(vwi_feature_last_checked);
            $vwi_feature_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_feature_last_checked.checked);
        }
        vwi_feature_last_checked = this;
    });

    //Multi-select handler for "Page or Post"
    var $vwi_page_chkboxes = $('.vwi_page:visible');
    var vwi_page_last_checked = null;
    $vwi_page_chkboxes.click(function(e) {
        if (!vwi_page_last_checked) {
            vwi_page_last_checked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $vwi_page_chkboxes.index(this);
            var end = $vwi_page_chkboxes.index(vwi_page_last_checked);
            $vwi_page_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_page_last_checked.checked);
        }
        vwi_page_last_checked = this;
    });

    //Multi-select handler for "Page or Post"
    var $vwi_post_chkboxes = $('.vwi_post:visible');
    var vwi_post_last_checked = null;
    $vwi_post_chkboxes.click(function(e) {
        if (!vwi_post_last_checked) {
            vwi_post_last_checked = this;
            return;
        }
        if (e.shiftKey) {
            var start = $vwi_post_chkboxes.index(this);
            var end = $vwi_post_chkboxes.index(vwi_post_last_checked);
            $vwi_post_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_post_last_checked.checked);
        }
        vwi_post_last_checked = this;
    });
}

/**
 *  @brief Import Multiple Pages - Invoked from Stage 4
 *  
 *  @return N/A
 *  
 *  @details Iterates all of the pages selected for Import and imports them
 *           
 */
function vwi_hook_import_pages_iterator($) {
    /*
     ** Iterate the list of pages to do
     */
    vwi_handle_fetch_pages_html($);
    $(document).ajaxStop(function() {

        $('#vwi_processing').text('Import process running... (pausing for breath)');
        setTimeout(function() {
            vwi_handle_fetch_pages_html($);
        }, vwi_breathing_space);

    });
}

/**
 *  @brief Import Menus - Invoked from Stage 2
 *  
 *  @return N/A
 *  
 *  @details Identifies menus in target URL
 *           
 */
function vwi_hook_identify_menus($) {
    $('#submit').attr('disabled', 'disabled');
    vwi_handle_fetch_menu_html($);
}

/**
 *  @brief Link Menus & Content - Update Menus
 *  
 *  @return N/A
 *  
 *  @details Links menus and content
 *           
 */
function vwi_hook_update_menus($) {
    vwi_handle_update_menus($);
}

/**
 *  @brief Update Internal Links
 *  
 *  @return N/A
 *  
 *  @details Updates internal links
 *           
 */
function vwi_hook_update_links($) {
    vwi_handle_update_links($);
}

/**
 *  @brief Localise Images - Hook Scan for Images - Invoked from Stage 2
 *  
 *  @return N/A
 *  
 *  @details Scans for images
 *           
 */
function vwi_hook_image_scan($) {
    // Handle bigger sites
    $('#vwi_form').submit(function() {
        var vwi_import_chkboxes = [];
        $('[id^=vwi_import]').each(function(){
            var entry = {id:this.id,checked:this.checked};
            vwi_import_chkboxes.push(entry);
        });
        var vwi_urls = [];
        $('[id^=vwi_url]').each(function(){
            var entry = {id:this.id,value:this.value};
            vwi_urls.push(entry);
        });
        var vwi_titles = [];
        $('[id^=vwi_title]').each(function(){
            var entry = {id:this.id,value:this.value};
            vwi_titles.push(entry);
        });
        var vwi_alts = [];
        $('[id^=vwi_alt]').each(function(){
            var entry = {id:this.id,value:this.value};
            vwi_alts.push(entry);
        });
        $('#vwi_json_chkboxes').val(JSON.stringify(vwi_import_chkboxes));
        $('#vwi_json_urls').val(JSON.stringify(vwi_urls));
        $('#vwi_json_titles').val(JSON.stringify(vwi_titles));
        $('#vwi_json_alts').val(JSON.stringify(vwi_alts));
        return true;
    });
    vwi_handle_scan_for_images($);
}

/**
 *  @brief Localise Images - Hook Scan for Images - Invoked from Stage 2
 *  
 *  @return N/A
 *  
 *  @details Scans for images
 *           
 */
function vwi_hook_image_import($) {
    vwi_handle_import_images($);
}

/**
 *                                  ###                             ###
 *                                  ### MAIN CONTROLLER FUNCTIONS   ###
 *                                  ###                             ###
 */

/**
 *  @brief Import Single Page - Fetch Page HTML
 *  
 *  @param [in] multi_page       Controls whether we are importing one page or many
 *  @param [in] vwi_url         The URL to fetch
 *  @param [in] vwi_main_image  Controls whether to set the featured image
 *  @param [in] vwi_page_type   Controls whether this is a Page or Post
 *  @param [in] vwi_publish     Controls whether to set article to Published or Draft status
 *  @return N/A
 *  
 *  @details This routine invokes an Ajax call to read the given URL
 *           This Ajax call will return base64 encoded HTML retrieved from the URL
 *           It will pass this on to the callback routine.
 */
function vwi_handle_fetch_page_html($, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    $.ajax({
        attempt: 0,
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_read_url',
            data_nonce: vwi_nonce,
            vwi_url: vwi_url,
        },
        success: function(response) {
            if(response.success){
                vwi_handle_fetch_page_html_callback($, response, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
            }else{
                this.attempt++;
                if (this.attempt <= vwi_retries) {
                    if(!multi_page){
                        $('#fetching_message').text("Retry attempt "+this.attempt+" of " + vwi_retries + "...");
                    }
                    $.ajax(this);
                }else{
                    vwi_handle_fetch_page_html_callback($, response, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
                }
            }          
        }
    });
}

/**
 *  @brief Import Single Page - Fetch Page HTML Callback
 *  
 *  @param [in] $                The jQuery object
 *  @param [in] response         The response returned from the last Ajax call
 *  @param [in] multi_page       Controls whether we are importing one page or many
 *  @param [in] vwi_url         The URL to fetch
 *  @param [in] vwi_main_image  Controls whether to set the featured image
 *  @param [in] vwi_page_type   Controls whether this is a Page or Post
 *  @param [in] vwi_publish     Controls whether to set article to Published or Draft status
 *  @return Return N/A
 *  
 *  @details Assuming sucess, updates the UI status before invoking the extraction of the page structure
 *           Otherwise, displays suitable error
 */
function vwi_handle_fetch_page_html_callback($, response, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    if (response.success) {
        if (!multi_page) {
            $('#fetching_status').attr('src', 'images/yes.png');
            $('#fetching_message').text('Done');
            $('#extract_status').attr('src', 'images/loading.gif');
        } else {
            $('#message_' + multi_page).text('Extracting content...');
        }
        vwi_handle_fetch_page_structure($, response.data, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
    } else {
        if (!multi_page) {
            $('#fetching_status').attr('src', 'images/no.png');
            $('#fetching_message').text(response.data);
        } else {
            $('#status_' + multi_page).attr('src', 'images/no.png');
            $('#message_' + multi_page).text(response.data);
        }
    }
}

/**
 *  @brief Import Single Page - Fetch Page Structure
 *  
 *  @param [in] $                jQuery object
 *  @param [in] data             The data from which the page structure is sought (base 64 encoded)
 *  @param [in] multi_page       Controls whether we are importing one page or many
 *  @param [in] vwi_url         The URL to fetch
 *  @param [in] vwi_main_image  Controls whether to set the featured image
 *  @param [in] vwi_page_type   Controls whether this is a Page or Post
 *  @param [in] vwi_publish     Controls whether to set article to Published or Draft status
 *  @return Return description
 *  
 *  @details Will make an Ajax call to vwi_ajax_extract_content before invoking callback handler
 */
function vwi_handle_fetch_page_structure($, data, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_extract_content',
            data_nonce: vwi_nonce,
            vwi_url: vwi_url,
            vwi_min_para_size: vwi_min_para_size,
            vwi_main_image: vwi_main_image,
            vwi_other_images: vwi_other_images,
            vwi_extraction_failure: vwi_extraction_failure,
            vwi_content: data,
        },
        success: function(response) {
            vwi_handle_fetch_page_structure_callback($, response, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
        }
    });
}

/**
 *  @brief Import Single Page - Fetch Page Structure Callback
 * 
 *  @param [in] $                jQuery object
 *  @param [in] response         The structure returned by the previous Ajax call
 *  @param [in] multi_page       Controls whether we are importing one page or many
 *  @param [in] vwi_url         The URL to fetch
 *  @param [in] vwi_main_image  Controls whether to set the featured image
 *  @param [in] vwi_page_type   Controls whether this is a Page or Post
 *  @param [in] vwi_publish     Controls whether to set article to Published or Draft status
 *  @return N/A
 *  
 *  @details Assuming sucess, updates the UI status before invoking the insertion of the article into WP
 *           Otherwise, displays suitable error
 */
function vwi_handle_fetch_page_structure_callback($, response, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    if (response.success) {
        if (!multi_page) {
            $('#extract_status').attr('src', 'images/yes.png');
            $('#extract_message').text('Done');
            $('#insert_status').attr('src', 'images/loading.gif');
        } else {
            $('#message_' + multi_page).text('Inserting page/post into Wordpress...');
        }
        vwi_handle_insert_page_or_post($, response.data, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
    } else {
        if (!multi_page) {
            $('#extract_status').attr('src', 'images/no.png');
            $('#extract_message').text(response.data);
        } else {
            $('#status_' + multi_page).attr('src', 'images/no.png');
            $('#message_' + multi_page).text(response.data);
        }
    }
}

/**
 *  @brief Import Single Page - Handle Insert Page or Post
 *  
 *  @param [in] $                 jQuery object
 *  @param [in] page_or_post_data The physical content to insert into WP
 *  @param [in] multi_page        Controls whether we are importing one page or many
 *  @param [in] vwi_url          The URL to fetch
 *  @param [in] vwi_main_image   Controls whether to set the featured image
 *  @param [in] vwi_page_type    Controls whether this is a Page or Post
 *  @param [in] vwi_publish      Controls whether to set article to Published or Draft status
 *  @return N/A
 *  
 *  @details Will make an Ajax call to vwi_ajax_insert_page_or_post before invoking callback handler
 */
function vwi_handle_insert_page_or_post($, page_or_post_data, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_insert_page_or_post',
            data_nonce: vwi_nonce,
            vwi_url: vwi_url,
            vwi_page_or_post_type: vwi_page_type,
            vwi_publish: vwi_publish,
            vwi_front_page: vwi_front_page,
            vwi_page_or_post_data: page_or_post_data,
            vwi_extraction_failure: vwi_extraction_failure,
        },
        success: function(response) {
            vwi_handle_insert_page_or_post_callback($, response, multi_page, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);
        }
    });
}

/**
 *  @brief Import Single Page - Handle Insert Page or Post Callback
 *  
 *  @param [in] $                 jQuery object
 *  @param [in] response          Details of the article inserted in prior Ajax call
 *  @param [in] multi_page        Controls whether we are importing one page or many
 *  @param [in] vwi_url          The URL to fetch
 *  @param [in] vwi_main_image   Controls whether to set the featured image
 *  @param [in] vwi_page_type    Controls whether this is a Page or Post
 *  @param [in] vwi_publish      Controls whether to set article to Published or Draft status
 *  @return N/A
 *  
 *  @details Assuming sucess, updates the UI status before displaying final summary of action
 *           Otherwise, displays suitable error
 */
function vwi_handle_insert_page_or_post_callback($, response, multi_page = false, vwi_url, vwi_main_image, vwi_page_type, vwi_publish) {
    if (response.success) {
        if (!multi_page) {
            $('#insert_status').attr('src', 'images/yes.png');
            if (response['data']['failed_content_extraction']) {
                if(!response['data']['failed_content_substituted']){
                    $('#insert_message').html('Done - but no content was extracted - empty page/post created.');
                }else{
                    $('#insert_message').html('Done - but no content could be determined - full source page content used instead.');
                }
            } else {
                $('#insert_message').html('Done');
            }

            $('#vwi_processing').text('Import process complete.');
            var final_word = '';
            if (response['data']['failed_content_extraction']) {
                if(!response['data']['failed_content_substituted']){
                    final_word += 'Content extraction failed - however, a blank page/post with title <em>' + response['data']['post_title'] + '</em> has been created. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit">View it</a>';
                }else{
                    final_word += 'Content extraction failed - however, full source content was used to create page/post with title <em>' + response['data']['post_title'] + '</em>. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit">View it</a>';
                }
            } else {
                final_word += 'A page/post with title <em>' + response['data']['post_title'] + '</em> has been created. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit">View it</a>';
            }

            $('#vwi_final_word_para').html(final_word);
            $('#vwi_final_word').removeAttr('hidden');
            //});
        } else {
            if (response['data']['failed_content_extraction']) {
                $('#status_' + multi_page).attr('src', 'images/media-button-image.gif');
                if(!response['data']['failed_content_substituted']){
                    $('#message_' + multi_page).html('Content extraction failed - however, a blank page/post with title <em>' + response['data']['post_title'] + '</em> has been created. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit" target="_review">View it</a>');
                }else{
                    $('#message_' + multi_page).html('Content extraction failed - however, full source content used to create page/post with title <em>' + response['data']['post_title'] + '</em>. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit" target="_review">View it</a>');
                }
            } else {
                $('#status_' + multi_page).attr('src', 'images/yes.png');
                $('#message_' + multi_page).html('A page/post with title <em>' + response['data']['post_title'] + '</em> has been created. <a href="post.php?post=' + response['data']['post_id'] + '&action=edit" target="_review">View it</a>');
            }
        }
    } else {
        if (!multi_page) {
            $('#insert_status').attr('src', 'images/no.png');
            $('#insert_message').text(response.data);
        } else {
            $('#status_' + multi_page).attr('src', 'images/no.png');
            $('#message_' + multi_page).text(response.data);
        }
    }
}

/**
 *  @brief Import Multiple Pages - Fetch Page Links
 *  
 *  @param [in] final_run           Controls whether to end recursion
 *  @return N/A
 *  
 *  @details Recursive routine to spider a website.
 *           Routine can make parallel Ajax calls to improve performance
 *           Routine utilises several global variables such as:
 *              vwi_pages={};              //List of all pages discovered
 *              vwi_todo_pages={};         //List of all pages left to review
 *              vwi_done_pages={};         //List of pages reviewed so far
 *              vwi_doing_pages={};        //List of pages in process of being reviewed
 *              vwi_rejected_pages={};     //List of pages rejected (eg don't have correct mime type)
 */
function vwi_handle_fetch_page_links($, final_run = false) {
    /* 
     ** While we still have links in our todo list that aren't in our done list
     ** we will pop up to "N" of them off the queue and request their links.
     ** When all of those requests complete, we'll either come back here and get the next "N"
     ** Or we'll check our depth, refresh the screen, re-prime the queue and repeat
     ** Or we'll exit as complete
     ** @TODO Reduce number of calls to MD5 function
     */
    // Ticket #183 - File Save/Reload
    if (vwi_final_run){
        final_run = true;
        if(Object.keys(vwi_todo_pages).length==1){
            vwi_handle_fetch_page_links_complete($);
        }
    }

    /*
     ** Refresh screen with what we're about to process
     */
    // Ticket #183 - File Save/Reload
    if (vwi_current_depth==1 || Object.keys(vwi_todo_pages).length > 1) {
        $('#lev' + vwi_current_depth + '_status').attr('src', 'images/loading.gif');
    }
    var vwi_replacement_html = '<table width="100%" id="vwi_pages"><tr><th align="left">Status</th><th align="left">URL</th><th align="left">Message</th></tr>';
    var odd = true;
    $.each(vwi_pages, function(key, value) {
        if (odd) {
            vwi_replacement_html += '<tr style="background-color: #e5e5e5;"><td><img id="link_' + MD5(key) + '"/></td><td>' + key + '</td><td id="message_' + MD5(key) + '"> </td></tr>';
        } else {
            vwi_replacement_html += '<tr><td><img id="link_' + MD5(key) + '"/></td><td>' + key + '</td><td id="message_' + MD5(key) + '"> </td></tr>';
        }
        odd = !odd;
    });
    vwi_replacement_html += '</table>';
    $('#vwi_pages').html(vwi_replacement_html);
    $.each(vwi_done_pages, function(key, value) {
        $('#link_' + MD5(key)).attr('src', 'images/yes.png');
    });
    $.each(vwi_rejected_pages, function(key, value) {
        $('#link_' + MD5(key)).attr('src', 'images/no.png');
        $('#message_' + MD5(key)).text(value);
    });

    // Ticket #183 - File Save/Reload
    if (vwi_current_depth > vwi_scan_depth && !final_run) {
        vwi_handle_fetch_page_links_complete($);
        return;
    }
    var i = 1;
    $.each(vwi_todo_pages, function(key, value) {
        if (i > vwi_parallelisation) {
            return false;
        }
        if (key in vwi_done_pages) {
            //Skip it as we've already done it
        } else {
            vwi_done_pages[key] = value; //Add to done pages
            i++; //Increment counter
            $.ajax({ //Fire off ajax request
                attempt: 0,
                finished: false,
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'vwi_ajax_read_url_links',
                    data_nonce: vwi_nonce,
                    vwi_url: key,
                },
                beforeSend: function() {
                    $('#link_' + MD5(key)).attr('src', 'images/loading.gif');
                    vwi_doing_pages[key] = true;
                },
                success: function(response) {
                    if (response.success) {
                        //Ticket #190 - Only increment percentage on pass/fail and not retry
                        vwi_pages_this_pass++;
                        this.finished=true;
                        $('#link_' + MD5(key)).attr('src', 'images/yes.png');
                        $('#message_' + MD5(key)).text("");
                        if (!final_run) {
                            $.each(response.data.links, function(index, value) {
                                vwi_pages[value.url] = value.text;
                            });
                        }
                        //Replace the link text with actual page title
                        vwi_pages[key] = response.data.page_title;
                    } else {
                        // An error occured. We can optionally retry a number of times.
                        this.attempt++;
                        if (this.attempt <= vwi_retries) {
                            $('#message_' + MD5(key)).text("Retry attempt "+this.attempt+" of " + vwi_retries + "...");
                            $.ajax(this);
                        }else{
                            this.finished=true;
                            $('#link_' + MD5(key)).attr('src', 'images/no.png');
                            $('#message_' + MD5(key)).text(response.data.error);
                            vwi_rejected_pages[response.data.page_url] = response.data.error;
                            //Ticket #190 - Only increment percentage on pass/fail and not retry
                            vwi_pages_this_pass++;
                        }
                        return;
                    };
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("Error callback ");
                },
                complete: function() {
                    if(this.finished){
                        delete vwi_doing_pages[key];
                        if (Object.keys(vwi_doing_pages).length == 0) {
                            vwi_percent_complete = parseInt(vwi_pages_this_pass / Object.keys(vwi_todo_pages).length * 100);
                            //Take a breath, then continue
                            $('#lev' + vwi_current_depth + '_message').text(vwi_percent_complete + '% complete. Pausing for breath...');
                            setTimeout(function() {

                                $('#lev' + vwi_current_depth + '_message').text(vwi_percent_complete + '% complete. Processing...');
                                //Any more pages at this level to do?
                                var more_todo = false;
                                $.each(vwi_todo_pages, function(key, value) {
                                    if (!(key in vwi_done_pages)) {
                                        more_todo = true;
                                        vwi_handle_fetch_page_links($, final_run);
                                        return false;
                                    }
                                });

                                if (!more_todo) {
                                    var discovered = Object.keys(vwi_pages).length;
                                    $('#lev' + vwi_current_depth + '_status').attr('src', 'images/yes.png');
                                    $('#lev' + vwi_current_depth + '_message').text('Done - discovered ' + (discovered - vwi_pages_so_far) + ' new pages');
                                    vwi_pages_so_far = discovered;

                                    //Reprime our todo list
                                    vwi_current_depth++;
                                    vwi_todo_pages = {};
                                    vwi_pages_this_pass = 0;
                                    $.each(vwi_pages, function(key, value) {
                                        if (!(key in vwi_done_pages)) {
                                            vwi_todo_pages[key] = value;
                                        }
                                    });
                                    if (Object.keys(vwi_todo_pages).length > 0) {
                                        if (vwi_current_depth <= vwi_scan_depth) {

                                            vwi_handle_fetch_page_links($);
                                        } else {
                                            if (!final_run) {
                                                // Ticket #183 - File Save/Reload
                                                vwi_final_run=true;    //Sets global var that can be saved in JSON
                                                vwi_handle_fetch_page_links($, true);
                                            } else {
                                                //We're done
                                                // Ticket #183 - File Save/Reload
                                                vwi_final_run=true;
                                                vwi_handle_fetch_page_links_complete($);
                                            }

                                        }
                                    } else {
                                        //We've run out of links
                                        // Ticket #183 - File Save/Reload
                                        vwi_final_run=true;
                                        vwi_handle_fetch_page_links_complete($);
                                    }
                                }

                            }, vwi_breathing_space);
                        }
                    }
                },
            });
        }
    });
}

/**
 *  @brief Import Multiple Pages - Fetch Page Links Callback
 *  
 *  @return N/A
 *  
 *  @details Once all page links have been harvested, update the UI and place results into hidden form element
 *           Turn the submit buttons back on
 */
function vwi_handle_fetch_page_links_complete($) {
    /*
     ** Clean up any unprocessed bits
     */
    // Ticket #183 - File Save/Reload
    $('#vwi_processing').text('Scan process complete.');
    var i = 1;
    while ($('#lev' + i + '_status').length) {
        if ($('#lev' + i + '_message').text() == '') {
            $('#lev' + i + '_message').text('Not required.');
            $('#lev' + i + '_status').attr('src', 'images/yes.png');
        }
        i++;
    }

    /*
     ** Place the results into an element we can post to the next page
     */
    var keys = Object.keys(vwi_pages),
        i, len = keys.length;
    keys.sort();
    var vwi_final_pages = {};
    for (i = 0; i < len; i++) {
        if (!(keys[i] in vwi_rejected_pages)) {
            vwi_final_pages[keys[i]] = encodeURIComponent(vwi_pages[keys[i]]);
        }
    }
    $('#vwi_final_pages').val(JSON.stringify(vwi_final_pages));

    /*
     ** Enable the form button
     */
    $('#submit_top').removeAttr('disabled');
    $('#submit_bottom').removeAttr('disabled');
    // Ticket #183 - File Save/Reload
    $('#quit_top').attr('disabled', 'disabled');
    $('#quit_bottom').attr('disabled', 'disabled');
}

/**
 *  @brief Import Multiple Pages - Fetch Pages HTML
 *  
 *  @return N/A
 *  
 *  @details Updates the UI with current progress
 *           Invokes vwi_handle_fetch_page_html for each page that needs retrieving
 *           Once all processed, provide final feedback to user
 */
function vwi_handle_fetch_pages_html($) {
    var found_one = false;
    var count = 1;
    $.each(vwi_to_do_list, function(key, value) {
        if (!(value in vwi_done_pages)) {
            vwi_percent_complete = parseInt(Object.keys(vwi_done_pages).length / Object.keys(vwi_to_do_list).length * 100);
            $('#vwi_processing').text('Import process running... (' + vwi_percent_complete + '% complete)');
            //Get the URL to process
            $('#status_' + value).attr('src', 'images/loading.gif');
            $('#message_' + value).text('Retrieving page...');
            vwi_url = vwi_to_do_data[value].vwi_url;
            vwi_main_image = vwi_to_do_data[value].vwi_feature;
            vwi_page_type = vwi_to_do_data[value].vwi_page_or_post;
            vwi_publish = vwi_to_do_data[value].vwi_publish;
            vwi_done_pages[value] = vwi_url;
            vwi_handle_fetch_page_html($, value, vwi_url, vwi_main_image, vwi_page_type, vwi_publish);

            //Exit our loop (we'll recurse back here later)
            found_one = true;
            count++;
            if (count > vwi_parallelisation) {
                return false;
            }
        }
    });
    if (!found_one) {
        $('#vwi_final_word').removeAttr('hidden');
        $('#vwi_processing').text('Import process complete.');
    }
}

/**
 *  @brief Import Menus - Fetch Menu HTML
 *  
 *  @return N/A
 *  
 *  @details Triggers Ajax request to retrieve HTML of target page
 *           Invokes vwi_handle_fetch_page_html for each page that needs retrieving
 *           Once all processed, provide final feedback to user
 */
function vwi_handle_fetch_menu_html($) {
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_read_url',
            data_nonce: vwi_nonce,
            vwi_url: vwi_url,
        },
        success: function(response) {
            vwi_handle_fetch_menu_html_callback($, response);
        }
    });
}

/**
 *  @brief Import Menus - Fetch Menu HTML Callback
 *  
 *  @return N/A
 *  
 *  @details Updates the UI with progress to date
 *           Goes on to fetch menu structure
 *           Once all processed, provide final feedback to user
 */
function vwi_handle_fetch_menu_html_callback($, response) {
    if (response.success) {
        $('#fetching_status').attr('src', 'images/yes.png');
        $('#fetching_message').text('Done');
        $('#menu_status').attr('src', 'images/loading.gif');
        vwi_handle_fetch_menu_structure($, response.data);
    } else {
        $('#fetching_status').attr('src', 'images/no.png');
        $('#fetching_message').text(response.data);
    }
}

/**
 *  @brief Import Menus - Fetch Menu Structure
 *  
 *  @return N/A
 *  
 *  @details Triggers Ajax request to fetch structure of menus from provided HTML
 */
function vwi_handle_fetch_menu_structure($, data) {
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_extract_menu',
            data_nonce: vwi_nonce,
            vwi_url: vwi_url,
            vwi_menu_density: vwi_menu_density,
            vwi_menu_size: vwi_menu_size,
            vwi_content: data,
        },
        success: function(response) {
            vwi_handle_fetch_menu_structure_callback($, response);
        }
    });
}

/**
 *  @brief #98 - Import Menus - Fetch Menu Structure Callback
 * 
 *  @param [in] $          jQuery object
 *  @param [in] response   The extracted menu structure retrieved in previous Ajax call
 *  @return N/A
 *  
 *  @details Assuming success, update progress and invoke routine to display structure
 *           Otherwise, surface any errors
 */
function vwi_handle_fetch_menu_structure_callback($, response) {
    if (response.success) {
        $('#menu_status').attr('src', 'images/yes.png');
        $('#menu_message').text('Done');
        $('#submit').removeAttr('disabled');
        vwi_handle_display_menu_structure($, response.data);
    } else {
        $('#menu_status').attr('src', 'images/no.png');
        $('#menu_message').text(response.data);
    }
}

/**
 *  @brief #98 - Import Menus - Display Menu Structure
 * 
 *  @param [in] $                jQuery object
 *  @param [in] menu_structure   The extracted menu structure retrieved in previous Ajax call
 *  @return N/A
 *  
 *  @details Updates the UI to include menu structures
 *           Invokes vwi_handle_display_menu for each menu
 */
function vwi_handle_display_menu_structure($, menu_structure) {
    var menus = $.parseJSON(menu_structure);
    var menu_count = 0;
    var html = "";
    $.each(menus, function(index, value) {
        menu_count++;
        html += "<div style='display: inline-block; vertical-align: top; margin:10px; padding: 10px; border:1px solid gray;'>";
        html += "<p><strong>Menu #" + menu_count + "</strong></p>";
        html += "<ul>";
        html += vwi_handle_display_menu($, value);
        html += "</ul>";
        html += "<hr/>";
        html += "<p><input type='checkbox' name='vwi_menu_import_checkbox_" + menu_count + "' id='vwi_menu_import_checkbox_" + menu_count + "' onchange='vwi_handle_import_menu_click(this," + menu_count + ");'>Import menu</input></p>";
        html += "<div name='vwi_menu_import_name_wrapper_" + menu_count + "' id='vwi_menu_import_name_wrapper_" + menu_count + "' hidden style='padding: 0 0px;'>";
        html += "Menu Name:<br/>";
        html += "<input type='input' name='vwi_menu_import_name_" + menu_count + "' id='vwi_menu_import_name_" + menu_count + "' style='width:100%'/>";
        html += "<input type='hidden' name='vwi_menu_import_content_" + menu_count + "' id='vwi_menu_import_content_" + menu_count + "' style='width:100%' value='" + JSON.stringify(value) + "'/>";
        html += "</div>";
        html += "</div>";
    });
    $("#vwi_menus").html(html);

    $("#vwi_menus").each(function() {
        var currentWidest = 0;
        $(this).children().each(function(i) {
            if ($(this).width() > currentWidest) {
                currentWidest = $(this).width();
            }
        });

        $(this).children().css({
            'width': currentWidest
        });

    });
}

/**
 *  @brief #98 - Import Menus - Handle Display Menu
 *  
 *  @param [in] $                jQuery object  
 *  @param [in] menus            The menu to display
 *  @return                      HTML representing the menu
 *  
 *  @details Simply creates the HTML to represent the menu
 */
function vwi_handle_display_menu($, menus) {
    var html = "";
    $.each(menus, function(index, value) {
        html += "<li>"
        html += "&#9657;".repeat(value.path);
        html += "<a href='" + value.link + "' target='__menulink'>";
        html += value.text;
        html += "</a>";
        html += "</li>";
    });
    return html;
}

/**
 *  @brief Import Menus - Handle Import Menu Click
 *  
 *  @param [in] the_checkbox         Which menu we're selecting for import
 *  @param [in] index                Counter
 *  @return N/A
 *  
 *  @details Upon clicking to import a particular menu, reveals "name" input box
 */
function vwi_handle_import_menu_click(the_checkbox, index) {
    if (the_checkbox.checked) {
        document.getElementById("vwi_menu_import_name_wrapper_" + index).hidden = false;
        document.getElementById("vwi_menu_import_name_" + index).required = true;
        document.getElementById("vwi_menu_import_name_" + index).width = '50px';
        document.getElementById("vwi_menu_import_name_" + index).focus();
    } else {
        document.getElementById("vwi_menu_import_name_wrapper_" + index).hidden = true;
        document.getElementById("vwi_menu_import_name_" + index).required = false;
    }
}

/**
 *  @brief Link Menus & Content - Update Menus
 *  
 *  @return N/A
 *  
 *  @details More details
 */
function vwi_handle_update_menus($) {
    $('#menus_status').attr('src', 'images/loading.gif');
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_update_menus',
            data_nonce: vwi_nonce,
        },
        success: function(response) {
            vwi_handle_update_menus_callback($, response);
        }
    });
}

/**
 *  @brief Link Menus & Content - Update Menus Callback
 *  
 *  @return N/A
 *  
 *  @details Updates UI with a summary of what was changed
 */
function vwi_handle_update_menus_callback($, response) {
    if (response.success) {
        $('#menu_status').attr('src', 'images/yes.png');
        $('#menu_message').html('Done.');
        var odd = true;
        var got_one = false;
        $.each(response.data.updated, function(page, link) {
            got_one = true;
            if (odd) {
                $('#updated').append('<tr valign="top" style="background-color: #e5e5e5;"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            } else {
                $('#updated').append('<tr valign="top"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            }
            odd = !odd;
        });
        if (!got_one) {
            $('#updated').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td></tr>');
        }

        var odd = true;
        var got_one = false;
        $.each(response.data.skipped, function(page, link) {
            got_one = true;
            if (odd) {
                $('#skipped').append('<tr valign="top" style="background-color: #e5e5e5;"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            } else {
                $('#skipped').append('<tr valign="top"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            }
            odd = !odd;
        });
        if (!got_one) {
            $('#skipped').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td></tr>');
        }

        var odd = true;
        var got_one = false;
        $.each(response.data.already, function(page, link) {
            got_one = true;
            if (odd) {
                $('#already').append('<tr valign="top" style="background-color: #e5e5e5;"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            } else {
                $('#already').append('<tr valign="top"><td>' + link.menu_name + '</td><td>' + link.menu_title + '</td></tr>');
            }
            odd = !odd;
        });
        if (!got_one) {
            $('#already').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td></tr>');
        }

    } else {
        if (response.data == 'No menu items were updated.') {
            $('#menu_status').attr('src', 'images/yes.png');
        } else {
            $('#menu_status').attr('src', 'images/no.png');
        }
        $('#menu_message').text(response.data);
    }
}

/**
 *  @brief Update Internal Links - Handle Update Links
 *  
 *  @return N/A
 *  
 *  @details Triggers Ajax request to update links
 */
function vwi_handle_update_links($, page_or_post_id) {
    $('#link_status').attr('src', 'images/loading.gif');
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_update_links',
            data_nonce: vwi_nonce,
        },
        success: function(response) {
            vwi_handle_update_links_callback($, response);
        }
    });
}

/**
 *  @brief Update Internal Links - Handle Update Links Callback
 *  
 *  @return N/A
 *  
 *  @details Updates UI with a summary of what was changed
 */
function vwi_handle_update_links_callback($, response) {
    if (response.success) {
        $('#link_status').attr('src', 'images/yes.png');
        $('#link_message').html('Done.');

        if ($.type(response.data.updated) == 'string') {
            $('#link_message').html(response.data.updated);
            $('#updated').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td><td>n/a</td></tr>');
        } else {
            //Dump the updated URLS
            var odd = true;
            $.each(response.data.updated, function(page, links) {
                $.each(links, function(index, link) {
                    if (odd) {
                        $('#updated').append('<tr valign="top" style="background-color: #e5e5e5;"><td><a href="post.php?post=' + link.page_id + '&action=edit" target="__edit">' + page + '</a></td><td>' + vwi_link(link.old_url) + '</td><td>' + vwi_link(link.new_url) + '</td></tr>');
                    } else {
                        $('#updated').append('<tr valign="top"><td><a href="post.php?post=' + link.page_id + '&action=edit" target="__edit">' + page + '</a></td><td>' + vwi_link(link.old_url) + '</td><td>' + vwi_link(link.new_url) + '</td></tr>');
                    }
                    odd = !odd;
                });
            });
        }

        //Dump the skipped URLS
        var odd = true;
        var got_one = false;
        $.each(response.data.skipped, function(page, links) {
            got_one = true;
            $.each(links, function(index, link) {
                if (odd) {
                    $('#skipped').append('<tr valign="top" style="background-color: #e5e5e5;"><td><a href="post.php?post=' + link.page_id + '&action=edit" target="__edit">' + page + '</a></td><td>' + vwi_link(link.url) + '</td><td>' + link.message + '</td></tr>');
                } else {
                    $('#skipped').append('<tr valign="top"><td><a href="post.php?post=' + link.page_id + '&action=edit" target="__edit">' + page + '</a></td><td>' + vwi_link(link.url) + '</td><td>' + link.message + '</td></tr>');
                }
                odd = !odd;
            });
        });
        if (!got_one) {
            $('#skipped').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td><td>n/a</td></tr>');
        }
    } else {
        if (response.data == 'No links were updated.') {
            $('#link_status').attr('src', 'images/yes.png');
        } else {
            $('#link_status').attr('src', 'images/no.png');
        }
        $('#link_message').text(response.data);
    }
}

/**
 *  @brief Localise Images - Handle Scan for Images
 *  
 *  @return N/A
 *  
 *  @details Invokes Ajax call to locate all images referenced in content
 */
function vwi_handle_scan_for_images($) {
    $('#image_status').attr('src', 'images/loading.gif');
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_scan_for_images',
            data_nonce: vwi_nonce,
        },
        success: function(response) {
            vwi_handle_scan_for_images_callback($, response);
        }
    });
}

/**
 *  @brief Localise Images - Handle Scan for Images Callback
 *  
 *  @return N/A
 *  
 *  @details Updates the UI with details of images.
 *           Defaults import checkbox depending on whether internal/external/already imported
 */
function vwi_handle_scan_for_images_callback($, response) {
    if (response.success) {
        $('#image_status').attr('src', 'images/yes.png');
        $('#image_message').html('Done.');

        if ($.type(response.data) == 'string') {
            $('#image_message').html(response.data);
            $('#available').append('<tr valign="top" style="background-color: #e5e5e5;"><td>n/a</td><td>n/a</td><td align="center">n/a</td><td align="center">n/a</td><td align="center">n/a</td></tr>');
        } else {
            var row_count = 1;
            //Dump the internal URLS
            var odd = true;
            $.each(response.data.internal, function(image, pages) {
                var page_text = "";
                var page_count = 0;
                var image_title = "-";
                var image_alt= "-";
                $.each(pages, function(index, page) {
                    if (page.title.length > image_title.length){
                        image_title=page.title;
                    }
                    if (page.alt.length > image_alt.length){
                        image_alt=page.alt;
                    }
                    page_text += page.page + "\n";
                    page_count++;

                });
                add_row($, odd, true, false, image, page_text, page_count, 'Internal',image_title,image_alt);
                odd = !odd;
                row_count++;
            });
            //Dump the external URLS
            $.each(response.data.external, function(image, pages) {
                var page_text = "";
                var page_count = 0;
                var image_title = "-";
                var image_alt= "-";
                $.each(pages, function(index, page) {
                    if (page.title.length > image_title.length){
                        image_title=page.title;
                    }
                    if (page.alt.length > image_alt.length){
                        image_alt=page.alt;
                    }
                    page_text += page.page + "\n";
                    page_count++;

                });
                add_row($, odd, false, false, image, page_text, page_count, 'External',image_title,image_alt);
                odd = !odd;
                row_count++;
            });
            //Dump the already done URLS
            $.each(response.data.previously, function(image, pages) {
                var page_text = "";
                var page_count = 0;
                var image_title = "-";
                var image_alt= "-";
                $.each(pages, function(index, page) {
                    if (page.title.length > image_title.length){
                        image_title=page.title;
                    }
                    if (page.alt.length > image_alt.length){
                        image_alt=page.alt;
                    }
                    page_text += page.page + "\n";
                    page_count++;

                });
                add_row($, odd, false, true, image, page_text, page_count, 'Already Imported',image_title,image_alt);
                odd = !odd;
                row_count++;
            });
        }

        //Multi-select handler for "Import"
        var $vwi_import_chkboxes = $('.vwi_import');
        var vwi_import_last_checked = null;
        $vwi_import_chkboxes.click(function(e) {
            if (!vwi_import_last_checked) {
                vwi_import_last_checked = this;
                return;
            }
            if (e.shiftKey) {
                var start = $vwi_import_chkboxes.index(this);
                var end = $vwi_import_chkboxes.index(vwi_import_last_checked);
                $vwi_import_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', vwi_import_last_checked.checked);
                $vwi_import_chkboxes.slice(Math.min(start, end), Math.max(start, end) + 1).trigger('change');
            }
            vwi_import_last_checked = this;
        });

    } else {
        if (response.data == 'No images were updated.') {
            $('#image_status').attr('src', 'images/yes.png');
        } else {
            $('#image_status').attr('src', 'images/no.png');
        }
        $('#image_message').text(response.data);
    }

    function add_row($, odd, is_checked, is_disabled, image, page_text, page_count, link_type, image_title, image_alt) {
        var row_text = "";
        if (odd) {
            row_text = '<tr height="100px" valign="middle" style="background-color: #e5e5e5;">';
        } else {
            row_text = '<tr height="100px" valign="middle">';
        }

        row_text += '<td><img src="' + image + '" style="max-height:100px;max-width:200px;" onerror="this.onerror=null;this.src=\'images/no.png\';this.height=20;this.title=\'Image not found\';jQuery(\'#vwi_import_' + row_count + '\').prop(\'disabled\',true);jQuery(\'#vwi_type_' + row_count + '\').text(\'Image is missing\');"/></td><td>' + vwi_link(image) + '</td><td align="center"><a title="' + page_text + '">' + page_count + '</a></td><td align="center" id=\'vwi_type_' + row_count + '\'>' + link_type + '</td>';

        if (is_checked)
            row_text += '<td align="center"><input class="vwi_import" type="checkbox" name="vwi_import_' + row_count + '" id="vwi_import_' + row_count + '" checked';
        else {
            row_text += '<td align="center"><input class="vwi_import" type="checkbox" name="vwi_import_' + row_count + '" id="vwi_import_' + row_count + '" ';
        }
        if (is_disabled) {
            row_text += ' disabled ';
        }
        row_text += '/>';
        row_text += '<input type="hidden" name="vwi_url_' + row_count + '" id="vwi_url_' + row_count + '" value="' + image + '"/>';
        row_text += '<input type="hidden" name="vwi_title_' + row_count + '" id="vwi_title_' + row_count + '" value="' + image_title + '"/>';
        row_text += '<input type="hidden" name="vwi_alt_' + row_count + '" id="vwi_alt_' + row_count + '" value="' + image_alt + '"/>';
        row_text += '</td></tr>';
        $('#available').append(row_text);
    }
}

/**
 *  @brief Localise Images - Handle Import Images
 *  
 *  @return N/A
 *  
 *  @details Iterates the images to import in batches
 */
function vwi_handle_import_images($) {
    /*
        ** Iterate the list of pages to do
     */
    vwi_handle_import_image($);
    $(document).ajaxStop(function() {
        //console.log("In stop function");
        if (vwi_index <= vwi_total) {
            $('#image_message').text('Import process running... (pausing for breath)');
        }
        setTimeout(function() {
            //$('#image_message').text('Import process running...');
            vwi_handle_import_image($);
        }, vwi_breathing_space);

    });
}

function vwi_handle_import_images_update_content($,offset,updated_pages,updated_refs){
    $.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
            action: 'vwi_ajax_update_images',
            data_nonce: vwi_nonce,
            vwi_image_list: JSON.stringify(vwi_image_list),
            vwi_offset: offset,
        },
        success: function(response) {
            if (response.success) {
                updated_pages=updated_pages+response.data.pages_updated;
                updated_refs=updated_refs+response.data.links_updated;
                if(response.data.page_count>0){
                    $('#content_message').html('In progress, checked '+(offset+response.data.page_count)+' pages/posts. Updated  '+updated_refs+' image references across '+ updated_pages + ' pages/posts...');
                    offset=offset+50;
                    vwi_handle_import_images_update_content($,offset,updated_pages,updated_refs);
                }else{
                    $('#content_status').attr('src', 'images/yes.png');
                    $('#content_message').html('Done. Updated  '+updated_refs+' image references across '+ updated_pages + ' pages/posts...');
                }
            } else {
                $('#content_status').attr('src', 'images/no.png');
                $('#content_message').html(response.data.error);
            }
        }
    });
}

/**
 *  @brief Localise Images - Handle Import Image
 *  
 *  @return N/A
 *  
 *  @details Invokes Ajax calls "N" times to import images
 */
function vwi_handle_import_image($) {
    var found_one = false;
    //console.log("In handle import image");
    if (vwi_index > vwi_total) {
        if (!vwi_content_updated) {
            vwi_content_updated = true;
            $('#image_status').attr('src', 'images/yes.png');
            $('#image_message').text('Done');
            $('#content_status').attr('src', 'images/loading.gif');
            $('#content_message').text('In progress...');

            //console.log("Updating content ref");
            vwi_handle_import_images_update_content($,0, 0, 0);
        }
    }
    for (var count = 0; count < vwi_parallelisation; count++) {

        if (vwi_index <= vwi_total) {
            $('#vwi_status_' + vwi_index).attr('src', 'images/loading.gif');

            vwi_percent_complete = parseInt(vwi_index / vwi_total * 100);
            $('#image_message').text('Import process running... (' + vwi_percent_complete + '% complete)');

            //console.log("Triggering image import");
            var vwi_url = $('#vwi_url_' + vwi_index).text();
            var vwi_alt=$('#vwi_alt_' + vwi_index).text();
            var vwi_title=$('#vwi_title_' + vwi_index).text();
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    action: 'vwi_ajax_import_images',
                    data_nonce: vwi_nonce,
                    vwi_url: vwi_url,
                    vwi_index: vwi_index,
                    vwi_alt: vwi_alt,
                    vwi_title: vwi_title,
                },
                success: function(response) {
                    vwi_handle_import_image_callback($, response);
                }
            });
            vwi_index++;

        } else {

        }
    }
}

/**
 *  @brief Localise Images - Handle Import Image Callback
 *  
 *  @return N/A
 *  
 *  @details Updates UI with result of an image's import status
 */
function vwi_handle_import_image_callback($, response) {
    //console.log("In import image callback");
    if (response.success) {
        $('#vwi_status_' + response.data.vwi_index).attr('src', 'images/yes.png');
        $('#vwi_message_' + response.data.vwi_index).text('Done');
        var vwi_data = {
            'old_url': response.data.old_url,
            'new_url': response.data.new_url,
            'attach_id': response.data.attach_id,
        };
        vwi_image_list[response.data.vwi_index] = vwi_data;
    } else {
        $('#vwi_status_' + response.data.vwi_index).attr('src', 'images/no.png');
        $('#vwi_message_' + response.data.vwi_index).text(response.data.error);
    }
}

/**
 *                                      ###                     ###
 *                                      ### UTILITY FUNCTIONS   ###
 *                                      ###                     ###
 */

 /**
 *  @brief vwi Link
 *  
 *  @param [in] link             A link (url) to be summarised
 *  @return Summarised link
 *  
 *  @details Where URLS are too long to diplay, this routine will take the first and last 35 chars.
 */
 function vwi_link(link) {
     var new_link = ""
    if (link.length > 70) {
        new_link = link.substring(0, 35) + "..." + link.substring(link.length - 37);
    } else {
        new_link = link;
    }
    return '<a href="' + link + '" target="__review" title="' + link + '">' + new_link + '</a>';
}
function handle_filter_previous(e){
    (function($){
        if($(e).prop('checked')==true){
            $(".vwi_url").each(function(){
                if($.inArray($(this).text(),vwi_existing)>-1){
                    $(this).closest("td").attr('previousfilter',false);
                }else{
                    $(this).closest("td").attr('previousfilter',true);            
                }            
            });
        }else{
            $(".vwi_url").each(function(){
                $(this).closest("td").attr('previousfilter',true);  
            });
        }
    })(jQuery);
    handle_filter_refresh();
}

function handle_filter_urls(e){
    (function($){
        $(".vwi_url").each(function(){
            $(this).closest("td").attr('urlfilter',false);
        });
        $(".vwi_url:containsi("+e.value+")").each(function(){
            $(this).closest("td").attr('urlfilter',true);
        });
    })(jQuery);
    handle_filter_refresh();
}

function handle_filter_titles(e){
    (function($){
        $(".vwi_title").each(function(){
            $(this).closest("td").attr('titlefilter',false);
        });
        $(".vwi_title:containsi("+e.value+")").each(function(){
            $(this).closest("td").attr('titlefilter',true);
        });
    })(jQuery);
    handle_filter_refresh();
}

function handle_filter_refresh(){
   (function($){
       $(".vwi_results").each(function(){
           if(
            $(this).find(".vwi_url").closest("td").attr("urlfilter")=="true" &&
            $(this).find(".vwi_url").closest("td").attr("previousfilter")=="true" &&
            $(this).find(".vwi_title").closest("td").attr("titlefilter")=="true"
            
            ){
                $(this).show();
            }else{
                $(this).hide();
                if($("#vwi_auto_unselect").prop('checked')==true){
                    $(this).find('.vwi_import').removeAttr('checked');
                    $(this).find('.vwi_import').trigger('change');
                }
            }
       });
   })(jQuery);
}

/**
 *  @brief vwi_handle_multi_page_stage_2_save
 *  
 *  @param [in] e The click event
 *  @return Return null
 *  
 *  @details Saves progress to a file
 *  @since v1.7.0
 */
function vwi_handle_multi_page_stage_2_save(e){
    (function($){
        e.preventDefault();  
        var filename = prompt("Filename","vwi-"+(new URL(vwi_url)).hostname + ".json");
        if(filename!=null){
            var combined = JSON.stringify({
                vwi_url:vwi_url,
                vwi_current_depth:vwi_current_depth,
                vwi_scan_depth:vwi_scan_depth,
                vwi_pages:vwi_pages,
                vwi_todo_pages:vwi_todo_pages,
                vwi_done_pages:vwi_done_pages,
                vwi_rejected_pages:vwi_rejected_pages,
                vwi_doing_pages:vwi_doing_pages,
                vwi_pages_so_far:vwi_pages_so_far,
                vwi_pages_this_pass:vwi_pages_this_pass,
                vwi_percent_complete:vwi_percent_complete,
                vwi_final_run:vwi_final_run
            },null,3);
            var blob = new Blob([combined], {type: "text/plain;charset=utf-8"});
            saveAs(blob, filename);
        }
    })(jQuery);
}

/**
 *  @brief vwi_handle_multi_page_stage_2_quit
 *  
 *  @param [in] e The click event
 *  @return null
 *  
 *  @details Quits processing of multi-page spidering
 *  @since v1.7.0
 */
function vwi_handle_multi_page_stage_2_quit(e){
    (function($){
        if(confirm('Are you sure you want to quit finding links?')){
            /*
            ** Place the results into an element we can post to the next page
            */
            var keys = Object.keys(vwi_pages),
                i, len = keys.length;
            keys.sort();
            var vwi_final_pages = {};
            for (i = 0; i < len; i++) {
                if (!(keys[i] in vwi_rejected_pages)) {
                    vwi_final_pages[keys[i]] = encodeURIComponent(vwi_pages[keys[i]]);
                    }
            }
            $('#vwi_final_pages').val(JSON.stringify(vwi_final_pages));
        }else{
            e.preventDefault();         
        }
    })(jQuery);
}

/**
 *  @brief MD5
 *  
 *  @param [in] s String to be MD5'd
 *  @return MD5'd string
 *  
 *  @details Used to generate unique refences for use as tag IDs
 */
function MD5(s) {
    function L(k, d) {
        return (k << d) | (k >>> (32 - d))
    }

    function K(G, k) {
        var I, d, F, H, x;
        F = (G & 2147483648);
        H = (k & 2147483648);
        I = (G & 1073741824);
        d = (k & 1073741824);
        x = (G & 1073741823) + (k & 1073741823);
        if (I & d) {
            return (x ^ 2147483648 ^ F ^ H)
        }
        if (I | d) {
            if (x & 1073741824) {
                return (x ^ 3221225472 ^ F ^ H)
            } else {
                return (x ^ 1073741824 ^ F ^ H)
            }
        } else {
            return (x ^ F ^ H)
        }
    }

    function r(d, F, k) {
        return (d & F) | ((~d) & k)
    }

    function q(d, F, k) {
        return (d & k) | (F & (~k))
    }

    function p(d, F, k) {
        return (d ^ F ^ k)
    }

    function n(d, F, k) {
        return (F ^ (d | (~k)))
    }

    function u(G, F, aa, Z, k, H, I) {
        G = K(G, K(K(r(F, aa, Z), k), I));
        return K(L(G, H), F)
    }

    function f(G, F, aa, Z, k, H, I) {
        G = K(G, K(K(q(F, aa, Z), k), I));
        return K(L(G, H), F)
    }

    function D(G, F, aa, Z, k, H, I) {
        G = K(G, K(K(p(F, aa, Z), k), I));
        return K(L(G, H), F)
    }

    function t(G, F, aa, Z, k, H, I) {
        G = K(G, K(K(n(F, aa, Z), k), I));
        return K(L(G, H), F)
    }

    function e(G) {
        var Z;
        var F = G.length;
        var x = F + 8;
        var k = (x - (x % 64)) / 64;
        var I = (k + 1) * 16;
        var aa = Array(I - 1);
        var d = 0;
        var H = 0;
        while (H < F) {
            Z = (H - (H % 4)) / 4;
            d = (H % 4) * 8;
            aa[Z] = (aa[Z] | (G.charCodeAt(H) << d));
            H++
        }
        Z = (H - (H % 4)) / 4;
        d = (H % 4) * 8;
        aa[Z] = aa[Z] | (128 << d);
        aa[I - 2] = F << 3;
        aa[I - 1] = F >>> 29;
        return aa
    }

    function B(x) {
        var k = "",
            F = "",
            G, d;
        for (d = 0; d <= 3; d++) {
            G = (x >>> (d * 8)) & 255;
            F = "0" + G.toString(16);
            k = k + F.substr(F.length - 2, 2)
        }
        return k
    }

    function J(k) {
        k = k.replace(/rn/g, "n");
        var d = "";
        for (var F = 0; F < k.length; F++) {
            var x = k.charCodeAt(F);
            if (x < 128) {
                d += String.fromCharCode(x)
            } else {
                if ((x > 127) && (x < 2048)) {
                    d += String.fromCharCode((x >> 6) | 192);
                    d += String.fromCharCode((x & 63) | 128)
                } else {
                    d += String.fromCharCode((x >> 12) | 224);
                    d += String.fromCharCode(((x >> 6) & 63) | 128);
                    d += String.fromCharCode((x & 63) | 128)
                }
            }
        }
        return d
    }
    var C = Array();
    var P, h, E, v, g, Y, X, W, V;
    var S = 7,
        Q = 12,
        N = 17,
        M = 22;
    var A = 5,
        z = 9,
        y = 14,
        w = 20;
    var o = 4,
        m = 11,
        l = 16,
        j = 23;
    var U = 6,
        T = 10,
        R = 15,
        O = 21;
    s = J(s);
    C = e(s);
    Y = 1732584193;
    X = 4023233417;
    W = 2562383102;
    V = 271733878;
    for (P = 0; P < C.length; P += 16) {
        h = Y;
        E = X;
        v = W;
        g = V;
        Y = u(Y, X, W, V, C[P + 0], S, 3614090360);
        V = u(V, Y, X, W, C[P + 1], Q, 3905402710);
        W = u(W, V, Y, X, C[P + 2], N, 606105819);
        X = u(X, W, V, Y, C[P + 3], M, 3250441966);
        Y = u(Y, X, W, V, C[P + 4], S, 4118548399);
        V = u(V, Y, X, W, C[P + 5], Q, 1200080426);
        W = u(W, V, Y, X, C[P + 6], N, 2821735955);
        X = u(X, W, V, Y, C[P + 7], M, 4249261313);
        Y = u(Y, X, W, V, C[P + 8], S, 1770035416);
        V = u(V, Y, X, W, C[P + 9], Q, 2336552879);
        W = u(W, V, Y, X, C[P + 10], N, 4294925233);
        X = u(X, W, V, Y, C[P + 11], M, 2304563134);
        Y = u(Y, X, W, V, C[P + 12], S, 1804603682);
        V = u(V, Y, X, W, C[P + 13], Q, 4254626195);
        W = u(W, V, Y, X, C[P + 14], N, 2792965006);
        X = u(X, W, V, Y, C[P + 15], M, 1236535329);
        Y = f(Y, X, W, V, C[P + 1], A, 4129170786);
        V = f(V, Y, X, W, C[P + 6], z, 3225465664);
        W = f(W, V, Y, X, C[P + 11], y, 643717713);
        X = f(X, W, V, Y, C[P + 0], w, 3921069994);
        Y = f(Y, X, W, V, C[P + 5], A, 3593408605);
        V = f(V, Y, X, W, C[P + 10], z, 38016083);
        W = f(W, V, Y, X, C[P + 15], y, 3634488961);
        X = f(X, W, V, Y, C[P + 4], w, 3889429448);
        Y = f(Y, X, W, V, C[P + 9], A, 568446438);
        V = f(V, Y, X, W, C[P + 14], z, 3275163606);
        W = f(W, V, Y, X, C[P + 3], y, 4107603335);
        X = f(X, W, V, Y, C[P + 8], w, 1163531501);
        Y = f(Y, X, W, V, C[P + 13], A, 2850285829);
        V = f(V, Y, X, W, C[P + 2], z, 4243563512);
        W = f(W, V, Y, X, C[P + 7], y, 1735328473);
        X = f(X, W, V, Y, C[P + 12], w, 2368359562);
        Y = D(Y, X, W, V, C[P + 5], o, 4294588738);
        V = D(V, Y, X, W, C[P + 8], m, 2272392833);
        W = D(W, V, Y, X, C[P + 11], l, 1839030562);
        X = D(X, W, V, Y, C[P + 14], j, 4259657740);
        Y = D(Y, X, W, V, C[P + 1], o, 2763975236);
        V = D(V, Y, X, W, C[P + 4], m, 1272893353);
        W = D(W, V, Y, X, C[P + 7], l, 4139469664);
        X = D(X, W, V, Y, C[P + 10], j, 3200236656);
        Y = D(Y, X, W, V, C[P + 13], o, 681279174);
        V = D(V, Y, X, W, C[P + 0], m, 3936430074);
        W = D(W, V, Y, X, C[P + 3], l, 3572445317);
        X = D(X, W, V, Y, C[P + 6], j, 76029189);
        Y = D(Y, X, W, V, C[P + 9], o, 3654602809);
        V = D(V, Y, X, W, C[P + 12], m, 3873151461);
        W = D(W, V, Y, X, C[P + 15], l, 530742520);
        X = D(X, W, V, Y, C[P + 2], j, 3299628645);
        Y = t(Y, X, W, V, C[P + 0], U, 4096336452);
        V = t(V, Y, X, W, C[P + 7], T, 1126891415);
        W = t(W, V, Y, X, C[P + 14], R, 2878612391);
        X = t(X, W, V, Y, C[P + 5], O, 4237533241);
        Y = t(Y, X, W, V, C[P + 12], U, 1700485571);
        V = t(V, Y, X, W, C[P + 3], T, 2399980690);
        W = t(W, V, Y, X, C[P + 10], R, 4293915773);
        X = t(X, W, V, Y, C[P + 1], O, 2240044497);
        Y = t(Y, X, W, V, C[P + 8], U, 1873313359);
        V = t(V, Y, X, W, C[P + 15], T, 4264355552);
        W = t(W, V, Y, X, C[P + 6], R, 2734768916);
        X = t(X, W, V, Y, C[P + 13], O, 1309151649);
        Y = t(Y, X, W, V, C[P + 4], U, 4149444226);
        V = t(V, Y, X, W, C[P + 11], T, 3174756917);
        W = t(W, V, Y, X, C[P + 2], R, 718787259);
        X = t(X, W, V, Y, C[P + 9], O, 3951481745);
        Y = K(Y, h);
        X = K(X, E);
        W = K(W, v);
        V = K(V, g)
    }
    var i = B(Y) + B(X) + B(W) + B(V);
    return i.toLowerCase()
};


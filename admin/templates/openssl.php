<?php
if(!extension_loaded('openssl')){
?>
<div style="background-color:#ffeb3b;padding:5px;">
<p><strong>Warning - No SSL capability detected!</strong></p>
<p>Your installation of PHP does not appear to have the <a href="https://www.openssl.org/" target="_php">OpenSSL</a> extension enabled.</p>
<p>You will <strong>only</strong> be able to use Vindi Website Importer to retrieve content from sites accessible via a URL starting with "http".</p>
<p>You will <strong>not</strong> be able to use Vindi Website Importer to retrieve content from sites which only support URLs starting with "https".</p>
<p>The plug-in <strong>may not be able to</strong>  follow redirects or links within imported content that begin with "https".</p>
<p>Please see <a href="http://php.net/manual/en/openssl.setup.php" target="_php">this link</a> for more details or 
<a href="https://www.vindiweb.com" target="_support">contact support</a>.</p> 
</div>
<hr>
<?php
}

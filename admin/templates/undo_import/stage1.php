<?php
/**
 *  @file stage1.php
 *  @brief Undo import stage 1 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
    return;
}
?>
<h1>Undo Imports - Stage 1 of 2</h1>
<hr>
<strong>Introduction</strong>
<p>This feature will remove content imported using the site importer.</p>
<p><em>This action cannot be undone!</em></p>
<hr>
<strong>Options</strong>
<form method="post">
    <p> <input type="checkbox" name="vwi_undo_pages" id="vwi_undo_pages"\> Remove ALL Pages/Posts created by Site Importer</p>
    <p> <input type="checkbox" name="vwi_undo_menus" id="vwi_undo_menus"\> Remove ALL Menus created by Site Importer</p>
    <p> <input type="checkbox" name="vwi_undo_images" id="vwi_undo_images"\> Remove ALL Images created by Site Importer</p>
    <hr>
    <p> <strong>Notes:</strong> 
    <ul>
        <li> - Menus will be removed if they contain <em>any</em> items referencing either a page created by Site Importer or the site from which pages created by Site Importer were sourced.</li>
        <li> - Images will be removed if they <em>are referenced by</em> any pages created by Site Importer (even if they were not originally created by Site Importer).</li>
    </ul>
    For this reason, if pages have already been removed, this feature will not be able to locate any menus or images to remove.
    <input type="hidden" name="stage" id="stage" value="2">
    <?php wp_nonce_field('remove_content'); ?>
    <?php submit_button('Remove Content'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
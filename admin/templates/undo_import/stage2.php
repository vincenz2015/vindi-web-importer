<?php
/**
 *  @file stage2.php
 *  @brief Undo import stage 2 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
    return;
}
?>
<h1>Undo Imports - Stage 2 of 2</h1>
<hr>
<strong>Results</strong>
<p><?php echo $pages_removed?> Pages/Posts have been removed</p>
<p><?php echo $menus_removed?> Menus have been removed</p>
<p><?php echo $images_removed?> Images have been removed</p>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>

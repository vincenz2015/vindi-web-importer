<?php
/**
 *  @file stage2.php
 *  @brief Menu Wizard Stage 2 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage1' )){
	return;
}
?>
<h1>Import Menu Wizard</h1>
<h2>Stage 2 of 3</h2>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="fetching_status" src="images/loading.gif"/></td>
      <td> <span id="fetching_text">Fetch web page</span></td>
      <td><span id="fetching_message"></span></td>
   </tr>
   <tr>
      <td><img id="menu_status" src="../wp-includes/images/blank.gif"/></td>
      <td> <span id="menu_text">Extract menus</span></td>
      <td><span id="menu_message"></span></td>
   </tr>
</table>
<hr>
<form method="post">
   <input type="hidden" id="vwi_menu" name="vwi_menu" value=""/>
   <input type="hidden" name="stage" id="stage" value="3">
   <div id="vwi_menus">&nbsp;</div>
   <?php wp_nonce_field('update_stage2'); ?>
   <?php submit_button("Import menu(s) >>"); ?>
</form>
<script type="text/javascript">
   var vwi_hook="vwi_hook_identify_menus";
   var vwi_url="<?php echo rtrim($_POST['vwi_url'],'/')?>";
   var vwi_menu_density="<?php echo $_POST['vwi_menu_density']?>";
   var vwi_menu_size="<?php echo $_POST['vwi_menu_size']?>";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
</script>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
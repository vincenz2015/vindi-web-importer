<?php
/**
 *  @file stage1.php
 *  @brief Menu Wizard Stage 1 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Import Menu Wizard</h1>
<h2>Stage 1 of 3</h2>
<hr>
<strong>Introduction</strong>
<p>This routine will scan a website and offer up a summary of all menu blocks discovered.</p>
<p>You will then be able to choose which menus to import into your wordpress installation.</p>
<hr>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'openssl.php');?>
<strong>Options</strong>
<form method="post">
   <p>URL to scan: <input type="url" required placeholder = "http://www.example.com" name="vwi_url" id="vwi_url" size="47" value="<?php echo get_option('vwi_url','')?>"\>
      <span style="color:blue;cursor: pointer;" title="
Enter a webiste address, including the leading http:// or https://
The tool will retrieve the web page found at this URL
It will then automatically identify anything which looks like a menu within it.">
      &#9432;</span>
   </p>
   <p>Min Menu Density: <input type="number" id="vwi_menu_density" name="vwi_menu_density" min="0" value="<?php echo get_option('vwi_menu_density','5')?>" required>
      <span style="color:blue;cursor: pointer;" title="
This is a measure of the link density required before some links are considered to be part of a new menu.
It may vary depending on how the menus are constructed on the particular website.
Consider raising this value if the results appear to have split a signle menu into multiple menus.
Consider lowering this value if the resultant menu appears to include items which were not part of the original menu.">
      &#9432;</span>
   </p>
   <p>Min Menu Size: <input type="number" id="vwi_menu_size" name="vwi_menu_size" min="1" value="<?php echo get_option('vwi_menu_size','3')?>" required>
      <span style="color:blue;cursor: pointer;" title="
This is a measure of how many items a valid menu should have. 
Consider lowering this value if small menus are being ignored.
Consider raising this value if stray sets of links are being picked up as a menu.">
      &#9432;</span>
   </p>
   <input type="hidden" name="stage" id="stage" value="2">
   <?php wp_nonce_field('update_stage1'); ?>
   <?php submit_button('Identify menu(s) >>'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
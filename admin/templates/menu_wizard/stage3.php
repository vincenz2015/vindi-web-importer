<?php
/**
 *  @file stage3.php
 *  @brief Menu Wizard Stage 3 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage2' )){
	return;
}
?>
<h1>Import Menu Wizard</h1>
<h2>Stage 3 of 3</h2>
<strong>Import process complete.</strong>
<hr>
<table width="100%">
   <tr>
      <th align="left" width="5%">Status</th>
      <th align="left" width="15%">&nbsp;</th>
      <th align="left" width="80%">Message</th>
   </tr>
   <?php foreach($results as $result) {?>
   <tr>
      <td><img src="<?php echo ($result['status']=='Success'?'images/yes.png':'images/no.png');?>"/></td>
      <td><?php echo $result['status'];?></td>
      <td><?php echo $result['message'];?></td>
   </tr>
   <?php }?>
</table>
<hr/>
<p>Note: Your menu(s) will not be visible on your website until you update their 'display location' in the menu settings.</p>
<p>Click the link(s) above to access the menu settings for individual menus.</p>
<p>Alternately, to access the settings of all of your imported menus, please <a href='nav-menus.php'>click here</a>.</p>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
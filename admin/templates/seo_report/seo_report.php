<?php
/**
 *  @file seo_report.php
 *  @brief SEO Report template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>SEO Report</h1>
<hr>
<strong>Introduction</strong>
<p>This feature lists the before and after URLs of all imported content.</p>
<hr>
<table width="100%" id="table">
   <tr>
      <th width="45%" align="left">Before Import</th>
      <th width="45%" align="left">After Import</th>
      <th width="10%" align="right"><button onclick="CopyToClipboard('table')">Copy to Clipboard</button></th>
   </tr>
   <?php
   foreach($seoresults as $result){
   ?>
   <tr>
      <td><?php echo $result['before']; ?></td>
      <td><?php echo $result['after']; ?></td>
      <td>&nbsp;</td>
   </tr>
   <?php
   }
   ?>
</table>
<script type="text/javascript">
    function CopyToClipboard(containerid) {
        if (document.selection) { 
            var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select().createTextRange();
            document.execCommand("Selected text copied to clipboard"); 
        } else if (window.getSelection) {
            var range = document.createRange();
             range.selectNode(document.getElementById(containerid));
             window.getSelection().addRange(range);
             document.execCommand("copy");
             alert("Table copied to clipboard") 
        }
    }
</script>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
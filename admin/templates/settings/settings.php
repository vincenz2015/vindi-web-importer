<?php
/**
 *  @file settings.php
 *  @brief Settings page template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
    return;
}
?>
<h1>Vindi Website Importer</h1>
<hr>
<strong>Welcome</strong>
<p>Welcome to the Vindi Website Importer.  This system will import any website including Adobe Catalyst website content, into Wordpress</p>
<hr>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'openssl.php');?>
<strong>Options</strong>
<p>Apply settings here for use in all of the Site Importer functionality.</p>
<form method="post">
    <p>Max Download Size (MB): 
      <input type="number" required name="vwi_max_download" id="vwi_max_download" value="<?php echo get_option('vwi_max_download',10) ?>" 
      size="47" min="1" max="1000"\>
      <span style="color:blue;cursor: pointer;" title="This setting controls the maximum file size that the plugin will download.
    Default value: 10MB">
      &#9432;</span>
    </p>
    <p>User Agent: <input type="text" required placeholder = "VindiWebSiteImporterBot/1.0 (+https://vwi.vindiweb.com)" name="vwi_user_agent"
     id="vwi_user_agent" value="<?php echo get_option('vwi_user_agent','VindiWebsiteImporterBot/1.0 (+https://vwi.vindiweb.com)') ?>" size="80"\>
    <span style="color:blue;cursor: pointer;" title="
The User Agent the plugin uses when requesting files from web servers.
   ">
      &#9432;</span>
    </p>
    <hr>
    <input type="hidden" name="action" id="action" value="update">
    <?php wp_nonce_field('update_settings'); ?>
    <?php submit_button('Update Settings'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file update_menu.php
 *  @brief Update Menu template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Update Menus</h1>
<hr>
<strong>Introduction</strong>
<p>This routine will link imported menus to mported pages/posts.</p>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="menu_status" src="images/loading.gif"/></td>
      <td> <span id="menu_text">Update Menus</span></td>
      <td><span id="menu_message"></span></td>
   </tr>
</table>
<hr>
<strong>Updated Menu Items</strong><br/>
<font size="1">
<div>
<table width="100%" id="updated">
<tr>
	<th align="left" width="25%">Menu Name</th>
	<th align="left" width="75%">Menu Title</th>
</tr>
</table>
</div>
</font>
<br/>
<strong>Skipped Menu Items</strong><br/>
<font size="1">
<div>
<table width="100%" id="skipped">
<tr>
	<th align="left" width="25%">Menu Name</th>
	<th align="left" width="75%">Menu Title</th>
</tr>
</table>
</div>
</font>
</font>
<br/>
<strong>Menu Items Already Updated</strong><br/>
<font size="1">
<div>
<table width="100%" id="already">
    <tr>
	<th align="left" width="25%">Menu Name</th>
	<th align="left" width="75%">Menu Title</th>
</tr>
</table>
</div>
</font>
<script type="text/javascript">
   var vwi_hook="vwi_hook_update_menus";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
</script> 
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
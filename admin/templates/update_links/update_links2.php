<?php
/**
 *  @file update_links.php
 *  @brief Update Links template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Update Internal Links</h1>
<hr>
<strong>Introduction</strong>
<p>This routine will update any links which still point to the site from which content was imported, updating them to point to the newly imported content.</p>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="link_status" src="images/loading.gif"/></td>
      <td> <span id="link_text">Update Links</span></td>
      <td><span id="link_message"></span></td>
   </tr>
</table>
<hr>
<strong>Updated Links</strong><br/>
<font size="1">
<div>
<table width="100%" id="updated">
<tr>
	<th align="left" width="30%">Page</th>
	<th align="left" width="35%">Old Link</th>
	<th align="left" width="35%">New Link</th>
</tr>
</table>
</div>
</font>
<br/>
<strong>Skipped Links</strong><br/>
<font size="1">
<div>
<table width="100%" id="skipped">
<tr>
	<th align="left" width="30%">Page</th>
	<th align="left" width="35%">Link</th>
	<th align="left" width="35%">Message</th>
</tr>
</table>
</div>
</font>
<script type="text/javascript">
   var vwi_hook="vwi_hook_update_links";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
</script> 
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file update_links.php
 *  @brief Update Links template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
global $wp;
$current_url = home_url( add_query_arg( array(), $wp->request ) );
$parts = parse_url($current_url);
?>
<h1>Update Internal Links</h1>
<hr>
<strong>Introduction</strong>
<p>This routine will update any links which still point to the site from which content was imported, updating them to point to the newly imported content.</p>
<p><b>Important:</b> Do not run this option until you can access the site via it's final URL.</p>
<p>You are currently accessing this page via host: <b><?php echo $parts['scheme'].'://'.$parts['host'].$parts['path'] ?></p></b>
<p>Links such as <b>https://www.originaldomain.com/old_page.html</b> will be changed to <b><?php echo $parts['scheme'].'://'.$parts['host'].$parts['path'] ?>/new_page</b></p>
<p>Proceed ONLY if this is what you want to do.</p>
<p>If you wish to simulate the final URL before deploying to the live server, please see these guides:</p>
<p>Unix: <a href="https://www.weg.ucar.edu/documentation/hostfile-unix.html">https://www.weg.ucar.edu/documentation/hostfile-unix.html</a></p>
<p>Windows (WAMP): <a href="https://john-dugan.com/wamp-vhost-setup/">https://john-dugan.com/wamp-vhost-setup/</a></p>
<p>Windows (XAMP): <a href="https://ultimatefosters.com/hosting/setup-a-virtual-host-in-windows-with-xampp-server/">https://ultimatefosters.com/hosting/setup-a-virtual-host-in-windows-with-xampp-server/</a></p>    
<p>Mac: <a href="https://www.imore.com/how-edit-your-macs-hosts-file-and-why-you-would-want">https://www.imore.com/how-edit-your-macs-hosts-file-and-why-you-would-want</a></p>
<p><b>Alternately, simply delay running this feature until after you have deployed to the final server location.</b></p>
<hr>
<form method="post">
	<input type="hidden" name="stage" id="stage" value="2">
	<?php wp_nonce_field('update_links1'); ?>
	<?php submit_button('I have read the warning - update links >>'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file internalise_images.php
 *  @brief Internalise Images template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
    return;
}
if (!check_admin_referer( 'update_stage2' )){
    return;
}
?>
<h1>Localise Images</h1>
<hr>
<h2>Stage 3 of 3</h2>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="image_status" src="images/loading.gif"/></td>
      <td> <span id="image_text">Importing Images</span></td>
      <td><span id="image_message">Preparing (waiting for browser images to load)...</span></td>
   </tr>
   <tr>
      <td><img id="content_status" src=""/></td>
      <td> <span id="content_text">Update content references</span></td>
      <td><span id="content_message"></span></td>
   </tr>   
</table>
<hr>
<font size="1">
<div>
<table width="100%" id="available">
<tr>
    <th align="left" width="10%">Thumb</th>
    <th align="left" width="60%">URL</th>
    <th align="center" width="10%">Status</th>
    <th align="center" width="30%">Message</th>
</tr>
<?php
$odd=true;
$counter=1;
$index=1;

//Convert posted data back to version 1.2 fomat
$vwi_json_chkboxes = json_decode(stripslashes($_POST['vwi_json_chkboxes']));
foreach($vwi_json_chkboxes as $item){
    if($item->checked==1){
        $_POST[$item->id]='on';
    }else{
        $_POST[$item->id]='';
    }
}
$vwi_json_urls = json_decode(stripslashes($_POST['vwi_json_urls']));
foreach($vwi_json_urls as $item){
    $_POST[$item->id]=$item->value;
}
$vwi_json_titles = json_decode(stripslashes($_POST['vwi_json_titles']));
foreach($vwi_json_titles as $item){
    $_POST[$item->id]=$item->value;
}
$vwi_json_alts = json_decode(stripslashes($_POST['vwi_json_alts']));
foreach($vwi_json_alts as $item){
    $_POST[$item->id]=$item->value;
}

while(isset($_POST['vwi_url_'.$counter])){
    if(isset($_POST['vwi_import_'.$counter]) && $_POST['vwi_import_'.$counter]=='on'){
        if($odd){
            echo '<tr height="100px" valign="middle" style="background-color: #e5e5e5;">';
        }else{
            echo '<tr height="100px" valign="middle">';
        }
        echo '<td><img src="'.$_POST['vwi_url_'.$counter].'" style="max-height:100px;max-width:200px;" </td>';
        echo '<td id="vwi_url_'.$index.'">'.$_POST['vwi_url_'.$counter].'</td>';
        echo '<td align="center"><img id="vwi_status_'.$index.'" src=""/>';
        echo '<span id="vwi_title_'.$index.'" style="display:none">'.$_POST['vwi_title_'.$counter].'</span>';
        echo '<span id="vwi_alt_'.$index.'" style="display:none">'.$_POST['vwi_alt_'.$counter].'</span>';
        echo '</td>';
        echo '<td id="vwi_message_'.$index.'">&nbsp;</td>';
        echo '<tr/>';
        $index++;
        $odd=!$odd;
    }
    $counter++;
}
?>
</table>
</div>
</font>
    <hr>
</form>
<script type="text/javascript">
   var vwi_hook="vwi_hook_image_import";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
   var vwi_parallelisation=<?php echo $_POST['vwi_parallelisation']?>;        //Number of pages to review in parallel
   var vwi_breathing_space=<?php echo $_POST['vwi_breathing_space']?>;        //Time to pause between requests 
   var vwi_index=1;
   var vwi_total=<?php echo $index-1?>;
   var vwi_image_list={};
   vwi_percent_complete=0;
   var vwi_content_updated=false;
</script> 
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file internalise_images.php
 *  @brief Internalise Images template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Localise Images</h1>
<hr>
<h2>Stage 1 of 3</h2>
<hr>
<strong>Introduction</strong>
<p>This routine will import any images referenced in the content and update the references to point to the newly imported image.</p>
<hr>
<form method="post">
    <p>Parallelisation: <input type="number" id="vwi_parallelisation" name="vwi_parallelisation" min="1" max="20" value="<?php echo get_option('vwi_parallelisation','5')?>" required>
      <span style="color:blue;cursor: pointer;" title="
Controls how many images are retrieved at once. 
Increase this setting to process more images at the same time.
WARNING: High settings may overwhelm the web server you are checking and could result in your activity being treated as a potential Distributed Denial of Service (DDOS) attack.">
      &#9432;</span>
    </p>
    <p>Breathing Space: <input type="number" id="vwi_breathing_space" name="vwi_breathing_space" min="0" max="10000" value="<?php echo get_option('vwi_breathing_space','500')?>" step="100" required>
      <span style="color:blue;cursor: pointer;" title="
Controls how long (in milliseconds) to pause between each parallel batches of requests. 
This helps to avoid swamping the web server you are checking.
WARNING: Low settings may overwhelm the web server you are checking and could result in your activity being treated as a potential Distributed Denial of Service (DDOS) attack.">
      &#9432;</span>
    </p>	
	<input type="hidden" name="stage" id="stage" value="2">
	<hr>
	<?php wp_nonce_field('update_stage1'); ?>
	<?php submit_button('Next >>'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file internalise_images.php
 *  @brief Internalise Images template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage1' )){
	return;
}
?>
<h1>Localise Images</h1>
<hr>
<h2>Stage 2 of 3</h2>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="image_status" src="images/loading.gif"/></td>
      <td> <span id="image_text">Identify Images In Content</span></td>
      <td><span id="image_message"></span></td>
   </tr>
</table>
<hr>
<strong>Available Images</strong><br/>
<p>Note: To select an option for a range of rows, hold down "shift" when clicking.</p>
<?php // submit_button('Next >>'); TODO = Re-enable button ?>
<font size="1">
<div>
<table width="100%" id="available">
<tr>
	<th align="left" width="10%">Thumb</th>
	<th align="left" width="60%">URL</th>
	<th align="center" width="10%">Page(s)</th>
	<th align="center" width="10%">Type</th>
	<th align="center" width="10%">Action</th>
</tr>
</table>
</div>
</font>
<form method="post" id="vwi_form">
	<input type="hidden" id="vwi_parallelisation" name="vwi_parallelisation" value="<?php echo $_POST['vwi_parallelisation']?>">
    <input type="hidden" id="vwi_breathing_space" name="vwi_breathing_space" value="<?php echo $_POST['vwi_breathing_space']?>">
	<input type="hidden" name="stage" id="stage" value="3">
    <input type="hidden" name="vwi_json_chkboxes" id="vwi_json_chkboxes" value="">
    <input type="hidden" name="vwi_json_urls" id="vwi_json_urls" value="">
    <input type="hidden" name="vwi_json_titles" id="vwi_json_titles" value="">
    <input type="hidden" name="vwi_json_alts" id="vwi_json_alts" value="">
	<hr>
	<?php wp_nonce_field('update_stage2'); ?>
	<?php submit_button('Next >>'); ?>
</form>
<script type="text/javascript">
   var vwi_hook="vwi_hook_image_scan";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
</script> 
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
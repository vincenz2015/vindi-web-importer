<?php
/**
 *  @file stage2.php
 *  @brief Page Wizard Stage 2 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage1' )){
	return;
}
?>
<h1>Import Page Wizard</h1>
<h2>Stage 2 of 2</h2>
<strong id="vwi_processing">Import process running...</strong>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <tr>
      <td><img id="fetching_status" src="images/loading.gif"/></td>
      <td> <span id="fetching_text">Fetch web page</span></td>
      <td><span id="fetching_message"></span></td>
   </tr>
   <tr>
      <td><img id="extract_status" src="../wp-includes/images/blank.gif"/></td>
      <td> <span id="extract_text">Extract content</span></td>
      <td><span id="extract_message"></span></td>
   </tr>
   <tr>
      <td valign="top"><img id="insert_status" src="../wp-includes/images/blank.gif"/></td>
      <td valign="top"> <span id="insert_text">Insert content</span></td>
      <td valign="top"><span id="insert_message"></span></td>
   </tr>
</table>
<hr>
<div id="vwi_final_word" hidden>
   <p id='vwi_final_word_para'>The page has been imported.</p>
   <p>Alternately, click to <a href='edit.php'>view all posts</a> or <a href='edit.php?post_type=page'>view all pages</a></p>
   <p>Note: If the text appears corrupt, please check the Character Set Encoding Detection in the <a href="admin.php?page=vwi">Settings</a></p>
</div>
<script type="text/javascript">
   var vwi_hook="vwi_hook_import_single_page";
   var vwi_url="<?php echo rtrim($_POST['vwi_url'],'/')?>";
   var vwi_min_para_size=<?php echo $_POST['vwi_min_para_size']?>;
   var vwi_page_type="<?php echo $_POST['vwi_import_type']?>";
   var vwi_front_page=<?php echo (isset($_POST['vwi_front_page'])?'true':'false')?>;
   var vwi_publish=<?php echo (isset($_POST['vwi_publish'])?'true':'false')?>;
   var vwi_main_image=<?php echo (isset($_POST['vwi_main_image'])?'true':'false')?>;
   var vwi_other_images=<?php echo (isset($_POST['vwi_other_images'])?'true':'false')?>;
   var vwi_update_menus=<?php echo (isset($_POST['vwi_update_menus'])?'true':'false')?>;
   var vwi_update_links=<?php echo (isset($_POST['vwi_update_links'])?'true':'false')?>;
   var vwi_extraction_failure="<?php echo $_POST['vwi_extraction_failure']?>";
   var vwi_timeout="<?php echo get_option('vwi_timeout',15)?>";
   var vwi_retries="<?php echo get_option('vwi_retries',0)?>";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
</script>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file stage1.php
 * Page Wizard Stage 1 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Import Page Wizard</h1>
<h2>Stage 1 of 2</h2>
<hr>
<strong>Introduction</strong>
<p>This routine will import the content of a web page into Wordpress.</p>
<p>It will ignore all elements of the target page which it does not consider to be content.</p>
<hr>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'openssl.php');?>
<strong>Options</strong>
<form method="post">
	<p>URL to scan: <input type="url" required placeholder = "http://www.example.com" name="vwi_url" id="vwi_url" size="47"\ value="<?php echo get_option('vwi_url','')?>">
      <span style="color:blue;cursor: pointer;" title="
Enter a webiste address, including the leading http:// or https://
The tool will retrieve the web page found at this URL
It will then automatically identify the content within it.">
      &#9432;</span>
	</p>
	<p>Import Type: 
		<input type="radio" id="vwi_import_type" name="vwi_import_type" value="Page" <?php echo(get_option('vwi_import_type','Page')=='Page'?'checked':'')?>>Page</input>
		<input type="radio" id="vwi_import_type" name="vwi_import_type" value="Post" <?php echo(get_option('vwi_import_type','Page')=='Post'?'checked':'')?>>Post</input>
		<span style="color:blue;cursor: pointer;" title="
Determines whether to import content as a Wordpress page or post.">
      &#9432;</span>
	</p>
	<p>Set as Front Page: <input type="checkbox" id="vwi_front_page" name="vwi_front_page" <?php echo(get_option('vwi_front_page','on')=='on'?'checked':'')?>>
      <span style="color:blue;cursor: pointer;" title="
Determines whether to make the newly imported page the front page of the Wordpress site.">
      &#9432;</span>
	</p> 
	<p>Set as Published: <input type="checkbox" id="vwi_publish" name="vwi_publish" <?php echo(get_option('vwi_publish','on')=='on'?'checked':'')?>>
      <span style="color:blue;cursor: pointer;" title="
Determines whether to set the newly imported page to a status of published rather than draft.">
      &#9432;</span>
	</p> 	
    <p>Min Para Size: <input type="number" id="vwi_min_para_size" name="vwi_min_para_size" min="1" value="<?php echo get_option('vwi_min_para_size','2')?>" required>
      <span style="color:blue;cursor: pointer;" title="
Some websites output small placeholder elements which are later replaced dynamically with special components.
These placeholders can get picked up by the extraction routine. This setting controls the minimum size (in characters) that a paragraph must reach to be imported.
Consider lowering this value if small paragraphs are being ignored.
Consider raising this value if odd text is appearing in the results.">
      &#9432;</span>
    </p>
	<p>Set featured image: <input type="checkbox" id="vwi_main_image" name="vwi_main_image" <?php echo(get_option('vwi_main_image','on')=='on'?'checked':'')?>>
      <span style="color:blue;cursor: pointer;" title="
If a dominant image (normally the first) can be identified on the webpage, this may be set as the page or post's featured image.">
      &#9432;</span>
	</p>
	<p>Keep content images: <input type="checkbox" id="vwi_other_images" name="vwi_other_images" <?php echo(get_option('vwi_other_images','on')=='on'?'checked':'')?>>
      <span style="color:blue;cursor: pointer;" title="
If ticked, all image tags in the content will be retained and can be localised later.
If unticked, all images will be stripped from the content.">
      &#9432;</span>
	</p>
	<p>Action on content extraction failure: 
      <span style="color:blue;cursor: pointer;" title="
Occasionally, Site Importer may not be able to determine the actual content on a web page.
This usually happens where the proportion of actual content on the page is very small.
You can choose to either create an empty 'place holder' page/post for such failures, take the full page content from the source or to skip the page creation entirely.
">
      &#9432;</span><br/> 
	<input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="blank_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='blank_page'?'checked':'')?>>Create Empty Page<br/>
    <input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="full_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='full_page'?'checked':'')?>>Use Full Page Content from Source<br/>
	<input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="skip_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='skip_page'?'checked':'')?>>Skip Page Creation
    
	</p>	
	<input type="hidden" name="stage" id="stage" value="2">
	<?php wp_nonce_field('update_stage1'); ?>
	<?php submit_button('Import page >>'); ?>
</form>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
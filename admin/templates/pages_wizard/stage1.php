<?php
/**
 *  @file stage1.php
 *  @brief Pages Wizard Stage 1 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
?>
<h1>Import Website</h1>
<h2>Stage 1 of 4</h2>
<hr>
<strong>Instructions</strong>
<p>This process will import the entire content of a website into Wordpress. It will attempt the following:</p>
<p>- The importer will look for a sitemap<br>
- The importer will attempt to load each page listed in the sitemap.  It will load the body content of each page, including all html tags and style classes.</p>
<hr>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'openssl.php');?>
<strong>Settings</strong>
<form method="post">
	<p>Enter a URL to scan: <input type="url" required placeholder = "http://www.example.com" name="vwi_url" id="vwi_url" size="47" value="<?php echo get_option('vwi_url','')?>"\>
      <span style="color:blue;cursor: pointer;" title="
Enter a webiste address, including the leading http:// or https://
The tool will retrieve the web page found at this URL
It will then look for the sitemap.">
      &#9432;</span>
	</p>
	<p><strong>OR</strong> Choose a File To Upload: <input type="file" name="vwi_file" id="vwi_file" accept="application/json"/>
      <span style="color:blue;cursor: pointer;" title="
If you have previously saved the results list from Stage 2, you can reload it here to avoid recrawling the entire site.">
      &#9432;</span><span id="file_upload_progress"></span>
	</p>
	<p><strong>OR</strong> Paste URLs to Import (one URL per line):<br/> <textarea name="vwi_url_list" id="vwi_url_list" rows="10" cols="100"><?php echo get_option('vwi_url_list','')?></textarea>
      <span style="color:blue;cursor: pointer;" title="
Paste a list of urls, one per line, and these pages will be imported.
Set Scan depth to 1 to avoid crawling each page for further pages.">
      &#9432;</span><span id="file_upload_progress"></span>
	</p>    
    <p>Scan depth: <input type="number" id="vwi_scan_depth" name="vwi_scan_depth" min="1" max="5" value="<?php echo get_option('vwi_scan_depth','2')?>" required>
      <span style="color:blue;cursor: pointer;" title="
Controls the depth to which the scan will run. 
A setting of 1 will only detect the entered url and all pages linked from that URL.
A setting of 2, will then also detect all pages linked from all of the above pages.
A setting of 3, will then also detect all pages linked from all of the above pages.
Note that the higher the setting, the more work the system needs to do and the longer it will take.">
      &#9432;</span>
    </p>
    <p>Parallelisation: <input type="number" id="vwi_parallelisation" name="vwi_parallelisation" min="1" max="20" value="<?php echo get_option('vwi_parallelisation','5')?>" required>
      <span style="color:blue;cursor: pointer;" title="
Controls how many pages are checked at once. 
Increase this setting to process more pages at the same time.
WARNING: High settings may overwhelm the web server you are checking and could result in your activity being treated as a potential Distributed Denial of Service (DDOS) attack.">
      &#9432;</span>
    </p>
    <p>Breathing Space: <input type="number" id="vwi_breathing_space" name="vwi_breathing_space" min="0" max="10000" value="<?php echo get_option('vwi_breathing_space','500')?>" step="100" required>
      <span style="color:blue;cursor: pointer;" title="
Controls how long (in milliseconds) to pause between each parallel batches of requests. 
This helps to avoid swamping the web server you are checking.
WARNING: Low settings may overwhelm the web server you are checking and could result in your activity being treated as a potential Distributed Denial of Service (DDOS) attack.">
      &#9432;</span>
    </p>	
	<input type="hidden" name="stage" id="stage" value="2">
    <input type="hidden" name="file_content" id="file_content" value="">
	<hr>
	<?php wp_nonce_field('update_stage1'); ?>
	<?php submit_button('Next >>'); ?>
</form>
<script>
    var vwi_file_content="";
    (function( $ ) {

    var reader = {};
    var file = {};
    //var slice_size = 1000 * 1024;
    var slice_size = 32 * 1024;

    function start_upload( event ) {
        //event.preventDefault();

        reader = new FileReader();
        file = document.querySelector( '#vwi_file' ).files[0];
        upload_file( 0 );
    }
    $( '#vwi_file' ).on( 'change', start_upload );

    function upload_file( start ) {
        var next_slice = start + slice_size + 1;
        var blob = file.slice( start, next_slice );
        reader.onloadend = function( event ) {
            if ( next_slice < file.size ) {
                var size_done = start + slice_size;
                var percent_done = Math.floor( ( size_done / file.size ) * 100 );
                jQuery('#file_upload_progress').text(" "+percent_done+"% uploaded");
                vwi_file_content+=reader.result;
                upload_file( next_slice );
            }else{
                jQuery('#file_upload_progress').text(" 100% uploaded");
                vwi_file_content+=reader.result;
                vwi_file_content=window.btoa(unescape(encodeURIComponent(vwi_file_content)));
                jQuery('#file_content').val(vwi_file_content);
            }
        }
        //reader.readAsDataURL( blob );
        reader.readAsText( blob );
        // At this point the file data is loaded to event.target.result
    };        
})( jQuery );
</script>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
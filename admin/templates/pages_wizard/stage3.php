<?php
/**
 *  @file stage3.php
 *  @brief Pages Wizard Stage 1 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage2' )){
	return;
}
?>
<h1>Import Multiple Pages</h1>
<h2>Stage 3 of 4</h2>
<hr>
<form method="post" id="vwi_form">
<table width="100%"><tr><td width="50%" style="border-right: 1px;border-right-style: groove;" >
<strong>Global Settings</strong>
<p>These settings will be applied to all pages imported:</p>
    <p>Min Para Size: <input type="number" id="vwi_min_para_size" name="vwi_min_para_size" min="1" value="2" required>
      <span style="color:blue;cursor: pointer;" title="
Some websites output small placeholder elements which are later replaced dynamically with special components.
These placeholders can get picked up by the extraction routine. This setting controls the minimum size (in characters) that a paragraph must reach to be imported.
Consider lowering this value if small paragraphs are being ignored.
Consider raising this value if odd text is appearing in the results.">
      &#9432;</span>
    </p>
	<p>Keep content images: <input type="checkbox" id="vwi_other_images" name="vwi_other_images" <?php echo(get_option('vwi_other_images','on')=='on'?'checked':'')?>>
      <span style="color:blue;cursor: pointer;" title="
If ticked, all image tags in the content will be retained and can be localised later.
If unticked, all images will be stripped from the content.">
      &#9432;</span>
	</p>
	<p>Action on content extraction failure: 
      <span style="color:blue;cursor: pointer;" title="
Occasionally, Site Importer may not be able to determine the actual content on a web page.
This usually happens where the proportion of actual content on the page is very small.
You can choose to either create an empty 'place holder' page/post for such failures, take the full page content from the source or to skip the page creation entirely.
">
      &#9432;</span><br/> 
	<input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="blank_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='blank_page'?'checked':'')?>>Create Empty Page<br/>
    <input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="full_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='full_page'?'checked':'')?>>Use Full Page Content from Source<br/>
	<input type="radio" id="vwi_extraction_failure" name="vwi_extraction_failure" value="skip_page" <?php echo(get_option('vwi_extraction_failure','blank_page')=='skip_page'?'checked':'')?>>Skip Page Creation
 	</p>	
</td>
<td width="50%" valign="top" style="padding-left:10px;">
    <strong>Filters</strong>
    <p>These settings allow you to filter the list below:</p>
	<p>Hide rows containing previously imported URLs: <input type="checkbox" id="vwi_filter_imported" name="vwi_filter_imported" onchange="handle_filter_previous(this)">
      <span style="color:blue;cursor: pointer;" title="
If ticked, any URLs in the list below which have already been imported will be hidden.">
      &#9432;</span>
    <p>Hide rows with URLs not containing: <input type="input" id="vwi_filter_urls" name="vwi_filter_urls" oninput="handle_filter_urls(this)">      
      <span style="color:blue;cursor: pointer;" title="
Begin typing and only rows with URLs containing the characters entered will be retained. 
The rest will be hidden. The search is case insensitive.">
      &#9432;</span>
	</p>
    <p>Hide rows with Titles not containing: <input type="input" id="vwi_filter_titles" name="vwi_filter_titles" oninput="handle_filter_titles(this)">
      <span style="color:blue;cursor: pointer;" title="
Begin typing and only rows with Titles containing the characters entered will be retained.
The rest will be hidden. The search is case insensitive.">
      &#9432;</span>
	</p>
	<p>Auto unselect hidden rows from import: <input type="checkbox" id="vwi_auto_unselect" name="vwi_auto_unselect" checked onchange="handle_filter_refresh()">
      <span style="color:blue;cursor: pointer;" title="
When ticked, rows which are hidden due to a filter being applied will automatically 
have their 'Import Content' setting cleared. If not ticked, rows will remember their 
original setting.">
      &#9432;</span>
	</p>    
</td>
</tr></table>
<hr>
<strong>Page Settings</strong>
<p>Note: To select an option for a range of rows, hold down "shift" when clicking.</p>
<?php submit_button('Next >>'); ?>
<font size="1">
<table width="100%">
<tr>
<th align="left" width="50%" valign="bottom">URL</th>
<th align="left" width="25%" valign="bottom">Page Title</th>
<th align="center" width="5%" valign="bottom"><span style="color:blue;cursor: pointer;" title="
Determines whether to import content from this page into Wordpress.
Hold down shift when clicking to select/unselect a range.">
      &#9432;</span><br/>Import<br/>Content</th>
<th align="center" width="10%" valign="bottom"><span style="color:blue;cursor: pointer;" title="
Determines whether to import content as a Wordpress page or post.">
      &#9432;</span><br/>Import<br/>Type</th>
<th align="center" width="5%" valign="bottom"><span style="color:blue;cursor: pointer;" title="
Determines whether to set the newly imported page to a status of published rather than draft.">
      &#9432;</span><br/>Set as<br/>Published</th>
<th align="center" width="5%" valign="bottom"><span style="color:blue;cursor: pointer;" title="
If a dominant image (normally the first) can be identified on the webpage, this may be set as the page or post's featured image.">
      &#9432;</span><br/>Featured<br/>Image</th>	  
</tr>
<?php
$vwi_pages = json_decode(stripcslashes($_POST['vwi_final_pages']));
$odd=true;
$counter=1;
foreach($vwi_pages as $url=>$title){
	if($odd){
		echo '<tr style="background-color: #e5e5e5;" class="vwi_results">';
	}else{
		echo '<tr class="vwi_results">';
	}
	$odd=!$odd;
?>
<td urlfilter="true" previousfilter="true"><span class="vwi_url"><?php echo $url?></span><input type="hidden" id="vwi_url_<?php echo($counter)?>" name="vwi_url_<?php echo($counter)?>" value="<?php echo($url)?>"/></td>
<td titlefilter="true"><span class="vwi_title"><?php echo urldecode($title)?></span></td>
<td align="center"><input type="checkbox" class="vwi_import" id="vwi_import_<?php echo($counter)?>" name="vwi_import_<?php echo($counter)?>" checked onchange="handle_import_selection(this,<?php echo($counter)?>);"></td>
<td align="center">
<input type="radio" class="vwi_page" id="vwi_page_<?php echo($counter)?>" name="vwi_page_or_post_<?php echo($counter)?>" value="page" checked/><span>Page</span>
<input type="radio" class="vwi_post" id="vwi_post_<?php echo($counter)?>" name="vwi_page_or_post_<?php echo($counter)?>" value="post"/><span>Post</span>
</td>
<td align="center"><input type="checkbox" class="vwi_publish" id="vwi_publish_<?php echo($counter)?>" name="vwi_publish_<?php echo($counter)?>" checked></td>
<td align="center"><input type="checkbox" class="vwi_feature" id="vwi_feature_<?php echo($counter)?>" name="vwi_feature_<?php echo($counter)?>"></td>
</tr>
<?php
	$counter++;
}
?>
</table>
</font>
	<input type="hidden" name="stage" id="stage" value="4">
	<input type="hidden" id="vwi_parallelisation" name="vwi_parallelisation" value="<?php echo $_POST['vwi_parallelisation']?>">
	<input type="hidden" id="vwi_breathing_space" name="vwi_breathing_space" value="<?php echo $_POST['vwi_breathing_space']?>">	
    <input type="hidden" id="vwi_json_urls" name="vwi_json_urls" value="">
    <input type="hidden" id="vwi_json_imports" name="vwi_json_imports" value="">
    <input type="hidden" id="vwi_json_page_or_posts" name="vwi_json_page_or_posts" value="">
    <input type="hidden" id="vwi_json_publishes" name="vwi_json_publishes" value="">
    <input type="hidden" id="vwi_json_features" name="vwi_json_features" value="">
	<?php wp_nonce_field('update_stage3'); ?>
	<?php submit_button('Next >>'); ?>
</form>
<script type="text/javascript">
   var vwi_hook="vwi_hook_multi_page_selector_control";
   function handle_import_selection(element,index){
	   if(element.checked){
		//Enable other elements
		jQuery("#vwi_publish_"+index).removeAttr("disabled");
		jQuery("#vwi_page_"+index).removeAttr("disabled");
		jQuery("#vwi_page_"+index).next("span").fadeTo(500,1);
		jQuery("#vwi_post_"+index).removeAttr("disabled");
		jQuery("#vwi_post_"+index).next("span").fadeTo(500,1);
		jQuery("#vwi_feature_"+index).removeAttr("disabled");
	   }else{
		//Disable other elements
		jQuery("#vwi_publish_"+index).attr("disabled",true); 
		jQuery("#vwi_page_"+index).attr("disabled",true);
 		jQuery("#vwi_page_"+index).next("span").fadeTo(500,0.4);
		jQuery("#vwi_post_"+index).attr("disabled",true);
		jQuery("#vwi_post_"+index).next("span").fadeTo(500,0.4);
		jQuery("#vwi_feature_"+index).attr("disabled",true);
	   }
   }
</script>
<script type="text/javascript">
    var vwi_existing = [
<?php
global $wpdb;
$r = $wpdb->get_col( $wpdb->prepare( "
        SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = 'vwi_source_url' 
        AND p.post_status <> 'trash' 
    ",""));
foreach($r as $vwi_existing){
    echo '"'.$vwi_existing.'",';
}
?>
];
</script>
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
<?php
/**
 *  @file stage4.php
 *  @brief Pages Wizard Stage 4 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage3' )){
	return;
}

//Convert posted data back to version 1.2 fomat
$vwi_json_urls = json_decode(stripslashes($_POST['vwi_json_urls']));
foreach($vwi_json_urls as $item){
    $_POST[$item->id]=$item->value;
}
$vwi_json_imports = json_decode(stripslashes($_POST['vwi_json_imports']));
foreach($vwi_json_imports as $item){
    if($item->checked==1){
        $_POST[$item->id]='on';
    }else{
        $_POST[$item->id]='';
    }
}
$vwi_json_page_or_posts = json_decode(stripslashes($_POST['vwi_json_page_or_posts']));
foreach($vwi_json_page_or_posts as $item){
    $_POST[$item->id]=$item->value;
}
$vwi_json_publishes = json_decode(stripslashes($_POST['vwi_json_publishes']));
foreach($vwi_json_publishes as $item){
    if($item->checked==1){
        $_POST[$item->id]='on';
    }else{
        $_POST[$item->id]='';
    }
}
$vwi_json_features = json_decode(stripslashes($_POST['vwi_json_features']));
foreach($vwi_json_features as $item){
    if($item->checked==1){
        $_POST[$item->id]='on';
    }else{
        $_POST[$item->id]='';
    }
}

/*
** Summarise the list to import_list
*/
$import_list=[];
$import_list_string='';
$import_list_js='';
$counter=1;
while(isset($_POST['vwi_url_'.$counter])){
	if(isset($_POST['vwi_import_'.$counter]) && $_POST['vwi_import_'.$counter]=='on'){
		$import_list[$counter]=true;
		$import_list_string.=','.$counter;
		$import_list_js.='vwi_to_do_data['.$counter.']={
vwi_url:"'.rtrim($_POST['vwi_url_'.$counter],'/').'",
vwi_page_or_post:"'.$_POST['vwi_page_or_post_'.$counter].'",
vwi_publish:'.((isset($_POST['vwi_publish_'.$counter]) && $_POST['vwi_publish_'.$counter]=='on')?'true':'false').',
vwi_feature:'.((isset($_POST['vwi_feature_'.$counter]) && $_POST['vwi_feature_'.$counter]=='on')?'true':'false').',
};
';
	}
	$counter++;
}
?>
<h1>Import Multiple Pages Wizard</h1>
<h2>Stage 4 of 4</h2>
<strong id="vwi_processing">Import process running...</strong>
<hr>
<font size="1">
<table width="100%">
   <tr>
      <th width="5%" align="left">Status</th>
      <th align="left">URL</th>
      <th align="left">Message</th>
   </tr>
<?php
$odd=true;
foreach($import_list as $key=>$value){
	if($odd){
		echo '<tr style="background-color: #e5e5e5;">';
	}else{
		'<tr>';
	}
?>
<td align="center"><img id="status_<?php echo $key?>"/></td>
<td id="vwi_url_<?php echo $key?>"><?php echo $_POST['vwi_url_'.$key]?></td>
<td><span id="message_<?php echo $key?>">&nbsp</span></td>
</tr>
<?php
	$odd=!$odd;
}
?>
</table>
</font>
<hr>
<div id="vwi_final_word" hidden>
   <p id='vwi_final_word_para'>The pages have been imported.</p>
   <p>To view a single page, click any of the links above.</p>
   <p>Alternately, click to <a href='edit.php'>view all posts</a> or <a href='edit.php?post_type=page'>view all pages</a></p>
</div>
<script type="text/javascript">
   var vwi_hook="vwi_hook_import_pages_iterator";
   var vwi_min_para_size = "<?php echo $_POST['vwi_min_para_size']?>";
   var vwi_publish='false';
   var vwi_main_image='false';
   var vwi_other_images=<?php echo (isset($_POST['vwi_other_images'])?'true':'false')?>;   
   var vwi_page_type="";
   var vwi_to_do_list=[<?php echo substr($import_list_string,1)?>];
   var vwi_to_do_data={};
   var vwi_done_pages={};
   var vwi_front_page=false;
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>";
   var vwi_url="";
   var vwi_extraction_failure="<?php echo $_POST['vwi_extraction_failure']?>";
   var vwi_ready_for_next=false; //Set to true when you want Ajx.stop handler to fire off next sequence
   var vwi_parallelisation=<?php echo $_POST['vwi_parallelisation']?>;		//Number of pages to review in parallel
   var vwi_breathing_space=<?php echo $_POST['vwi_breathing_space']?>;		//Time to pause between requests   
   var vwi_timeout="<?php echo get_option('vwi_timeout',15)?>";
   var vwi_retries="<?php echo get_option('vwi_retries',0)?>";
<?php echo $import_list_js?>
</script> 
<?php include(plugin_dir_path(dirname(__FILE__)) . 'footer.php');?>
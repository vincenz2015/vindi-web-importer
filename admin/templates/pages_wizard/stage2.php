<?php
/**
 *  @file stage2.php
 *  @brief Pages Wizard Stage 2 template
 */

/**
 *  Ensure file called appropriately
 */
if (!current_user_can('edit_theme_options')) {
	return;
}
if (!check_admin_referer( 'update_stage1' )){
	return;
}
?>
<h1>Import Multiple Pages</h1>
<h2>Stage 2 of 4</h2>
<strong id="vwi_processing">Scan process running...</strong>
<hr>
<table width="100%">
   <tr>
      <th width="10%" align="left">Status</th>
      <th width="15%" align="left">Action</th>
      <th width="75%" align="left">Message</th>
   </tr>
   <?php for($i=1;$i<=$_POST['vwi_scan_depth'];$i++){?>
   <tr>
      <td><img id="lev<?php echo $i;?>_status" src="../wp-includes/images/blank.gif"/></td>
      <td> <span id="lev<?php echo $i;?>_text">Perform level <?php echo $i;?> scan</span></td>
      <td><span id="lev<?php echo $i;?>_message"></span></td>
   </tr>
   <?php }?>
     <tr>
      <td><img id="lev<?php echo $i;?>_status" src="../wp-includes/images/blank.gif"/></td>
      <td> <span id="lev<?php echo $i;?>_text">Final checks</span></td>
      <td><span id="lev<?php echo $i;?>_message"></span></td>
   </tr>
</table>
<hr>
<form method="post">
	<input type="hidden" name="stage" id="stage" value="3">
	<input type="hidden" name="vwi_final_pages" id="vwi_final_pages" value="">
	<input type="hidden" id="vwi_parallelisation" name="vwi_parallelisation" value="<?php echo $_POST['vwi_parallelisation']?>">
	<input type="hidden" id="vwi_breathing_space" name="vwi_breathing_space" value="<?php echo $_POST['vwi_breathing_space']?>">
	<?php wp_nonce_field('update_stage2'); ?>
    <table><tr>
    <td>
    <?php submit_button('Save...','primary','save_top',true,array('onclick'=>'vwi_handle_multi_page_stage_2_save.call(this,event);','title'=>'Saves progress to a file which can be loaded in Step 1 to avoid reprocessing')); ?>
    </td><td>
    <?php submit_button('Quit...','primary','quit_top',true,array('onclick'=>'vwi_handle_multi_page_stage_2_quit.call(this,event);','title'=>'Abandons further processing and skips to stage 3')); ?>
    </td><td>
	<?php submit_button('Next >>','primary','submit_top'); ?>
    </td>
    </tr></table>
<hr>
<p><strong id="vwi_processing">Pages Discovered...</strong></p>
<font size="1"><p>
<table width="100%" id="vwi_pages">
   <tr>
      <th width="5%" align="left">Status</th>
      <th width="45%" align="left">Page Title</th>
      <th width="50%" align="left">Message</th>
   </tr>
</table>
</p></font>
<hr>
<table><tr>
<td>
<?php submit_button('Save...','primary','save_bottom',true,array('onclick'=>'vwi_handle_multi_page_stage_2_save.call(this,event);','title'=>'Saves progress to a file which can be loaded in Step 1 to avoid reprocessing')); ?>
</td><td>
<?php submit_button('Quit...','primary','quit_bottom',true,array('onclick'=>'vwi_handle_multi_page_stage_2_quit.call(this,event);','title'=>'Abandons further processing and skips to stage 3')); ?>
</td><td>
<?php submit_button('Next >>','primary','submit_bottom'); ?>
</td>
</tr></table>
<hr>
</form>
<script type="text/javascript">
   var vwi_hook="vwi_hook_spider_pages";
   var vwi_url="<?php echo rtrim($_POST['vwi_url'],'/')?>";
   var vwi_scan_depth="<?php echo $_POST['vwi_scan_depth']?>";
   var vwi_current_depth=1;
   var vwi_parallelisation=<?php echo $_POST['vwi_parallelisation']?>;		//Number of pages to review in parallel
   var vwi_breathing_space=<?php echo $_POST['vwi_breathing_space']?>;		//Number of pages to review in parallel
   var vwi_pages={};				//List of all pages discovered
   var vwi_todo_pages={};			//List of all pages left to review
   var vwi_done_pages={};			//List of pages reviewed so far
   var vwi_doing_pages={};			//List of pages in process of being reviewed
   var vwi_rejected_pages={};		//List of pages rejected (eg don't have correct mime type)
   var vwi_pages_so_far=0;
   var vwi_pages_this_pass=0;
   var vwi_percent_complete=0;
   var vwi_timeout="<?php echo get_option('vwi_timeout',15)?>";
   var vwi_retries="<?php echo get_option('vwi_retries',0)?>";
   var vwi_final_run=false;
   vwi_pages[vwi_url]="Home";
   var vwi_nonce="<?php echo wp_create_nonce('vwi');?>"; 
   <?php if($_POST['file_content']>''){?>
   var file_content = <?php echo base64_decode($_POST['file_content'])?>;
   vwi_url = file_content.vwi_url;
   vwi_current_depth = file_content.vwi_current_depth;
   vwi_scan_depth = file_content.vwi_scan_depth;
   vwi_pages = file_content.vwi_pages;
   vwi_todo_pages = file_content.vwi_todo_pages;
   vwi_done_pages = file_content.vwi_done_pages;
   //vwi_doing_pages = file_content.vwi_doing_pages;
   vwi_rejected_pages = file_content.vwi_rejected_pages;
   vwi_pages_so_far = file_content.vwi_pages_so_far;
   vwi_pages_this_pass = file_content.vwi_pages_this_pass;
   vwi_percent_complete = file_content.vwi_percent_complete;
   vwi_final_run = file_content.vwi_final_run;
   <?php }?>
   <?php 
    if($_POST['vwi_url_list']>''){
    $url_list=explode(PHP_EOL,$_POST['vwi_url_list']);
    if(is_array($url_list)){
		$specialChars = array(" ", "\r", "\n");
		$replaceChars = array("", "", "");
        echo PHP_EOL."// Paste list overrides".PHP_EOL;
        echo "   vwi_pages={};".PHP_EOL;
        echo "   vwi_url='".str_replace($specialChars,$replaceChars,$url_list[0])."';".PHP_EOL;
        foreach($url_list as $url){
            $url = str_replace($specialChars, $replaceChars, $url);
            echo "   vwi_pages['$url']='';".PHP_EOL;
            echo "   vwi_done_pages['$url']='';".PHP_EOL;
        }
        echo "   vwi_final_run=true;".PHP_EOL;
        echo "   vwi_current_depth=2;".PHP_EOL;
    }
       ?>
   
   <?php }?>
</script>
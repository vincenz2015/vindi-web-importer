<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://vindiweb.com
 * @since      1.0.0
 *
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/admin
 * @author     vince <vince@vindiweb.co.nz>
 */
class Vindi_Web_Importer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$protocol = 'https';
        if(!extension_loaded('openssl')){
            $protocol='http';
        }
         
        $this->service_endpoints = array(
            'extract_content'   =>  $protocol.'://www.vindiweb.com/webimporter/v1/extract_content/',
/*            'update_check'      =>  $protocol.'://www.vindiweb.com/webservice/v1/vwi_update_check/',*/
            );

        $this->max_download = get_option('vwi_max_download',10)*1024*1024;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vindi_Web_Importer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vindi_Web_Importer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/vindi-web-importer-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Vindi_Web_Importer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Vindi_Web_Importer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/vindi-web-importer-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function vwi_admin_menu()
    {
        add_menu_page('Web Importer', 'Web Importer', 'manage_options', 'vwi', array(
            $this,
            'vwi_settings'
        ) , 'dashicons-media-text');
        add_submenu_page('vwi', 'Settings', 'Settings', 'manage_options', 'vwi', array(
            $this,
            'vwi_settings'
        ));
        add_submenu_page('vwi', 'WP Website Import', 'Import Website', 'manage_options', 'vwi_import', array(
            $this,
            'vwi_import_website'
        ));
	}
	
	public function vwi_settings()
    {

        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
		}
		
		// call the handler
		if (isset($_POST['action']) && $_POST['action'] == 'update') {
			if (!check_admin_referer('update_settings')) {
				return;
			}

			$this->persist_settings(
				array(
					'vwi_max_download'=>$_POST['vwi_max_download'],
					'vwi_user_agent'=>$_POST['vwi_user_agent'],
					));
		}

		require_once (plugin_dir_path(dirname(__FILE__))
		. 'admin/templates/settings/settings.php');

	}

	public function vwi_import_website()
    {

        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        if(!isset($_POST['stage'])){
            $_POST['stage']='1';
        }
        switch ($_POST['stage']) {
        case '4':
            $this->persist_settings(
                array(
                    'vwi_min_para_size'=>$_POST['vwi_min_para_size'],
                    'vwi_other_images'=>$_POST['vwi_other_images'],
                    'vwi_extraction_failure'=>$_POST['vwi_extraction_failure']
                    ));
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/pages_wizard/stage4.php');
            break;

        case '3':
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/pages_wizard/stage3.php');
            break;

        case '2':   
            $this->persist_settings(
                array(
                    'vwi_url'=>$_POST['vwi_url'],
                    'vwi_scan_depth'=>$_POST['vwi_scan_depth'],
                    'vwi_parallelisation'=>$_POST['vwi_parallelisation'],
                    'vwi_breathing_space'=>$_POST['vwi_breathing_space']
                    ));
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/pages_wizard/stage2.php');
            break;

        default:
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/pages_wizard/stage1.php');
            break;
        }
    }   
	
    /* Import Menus */
    public function vwi_menu_wizard()
    {

        // check user capabilities

        if (!current_user_can('edit_theme_options')) {
            return;
        }

        if(!isset($_POST['stage'])){
            $_POST['stage']='1';
        }
        switch ($_POST['stage']) {
        case '3':
            if (!check_admin_referer('update_stage2')) {
                return;
            }

            $results = $this->vwi_create_wp_menu_set($_POST);
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/menu_wizard/stage3.php');
            break;

        case '2':
            $this->persist_settings(
                array(
                    'vwi_url'=>$_POST['vwi_url'],
                    'vwi_menu_density'=>$_POST['vwi_menu_density'],
                    'vwi_menu_size'=>$_POST['vwi_menu_size']
                    ));     
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/menu_wizard/stage2.php');
            break;

        default:
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/menu_wizard/stage1.php');
            break;
        }
    }
    
    /*
      Link Menus with Content
     */
    public function vwi_menu_update()
    {

        // check user capabilities

        if (!current_user_can('manage_options')) {
            return;
        }

        require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/update_menu/update_menu.php');

    }

    /*
     Update Internal Links
     */
    public function vwi_link_update()
    {

        // check user capabilities

        if (!current_user_can('manage_options')) {
            return;
        }
        if(!isset($_POST['stage'])){
            $_POST['stage']='1';
        }
        switch ($_POST['stage']) {
        case '2':
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/update_links/update_links2.php');
            break;        
        default:
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/update_links/update_links1.php');
            break;
        }    
    }
    
    /*
    Localise Images
     */ 
    public function vwi_image_wizard()
    {

        // check user capabilities

        if (!current_user_can('manage_options')) {
            return;
        }
        
        if(!isset($_POST['stage'])){
            $_POST['stage']='1';
        }
        switch ($_POST['stage']) {
        case '3':
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/images_wizard/step3.php');
            break;

        case '2':
            $this->persist_settings(
                array(
                    'vwi_parallelisation'=>$_POST['vwi_parallelisation'],
                    'vwi_breathing_space'=>$_POST['vwi_breathing_space']
                    ));     
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/images_wizard/step2.php');
            break;

        default:
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/images_wizard/step1.php');
            break;
        }
    }

    /*
    Undo Import Feature
     */ 
    public function vwi_undo_import()
    {
        // check user capabilities

        if (!current_user_can('manage_options')) {
            return;
        }

        if(!isset($_POST['stage'])){
            $_POST['stage']='1';
        }
        switch ($_POST['stage']) {
        case '2':
            $pages_removed=0;
            $menus_removed=0;
            $images_removed=0;
            if(isset($_POST['vwi_undo_images'])){
                $images_removed = $this->vwi_remove_images();
            }
            if(isset($_POST['vwi_undo_menus'])){
                $menus_removed = $this->vwi_remove_menus();
            }
            if(isset($_POST['vwi_undo_pages'])){
                $pages_removed = $this->vwi_remove_pages();
            }
            
            ///////PING IT HOME START
            $data = array(
                'vwi_api_user' => get_option('vwi_api_user') ,
                'vwi_api_key' => get_option('vwi_api_key') ,
                'vwi_url' => get_option('vwi_url') ,
                'pages_removed' => $pages_removed,
                'menus_removed' => $menus_removed,
                'images_removed' => $images_removed,
            );

            // Queryise it
            $post_data = http_build_query($data);

            // Build our post header converted to CURL
            $opts = array(
                CURLOPT_CUSTOMREQUEST=>'POST',
                CURLOPT_POST=>true,
                CURLOPT_POSTFIELDS=>$post_data,
            );

            // Turn that into a stream context @TODO - extend caching to this also???

            $fetched = $this->file_get_contents_curl($this->service_endpoints['undo_import'], null, $opts);
            $content = $fetched['content'];
            ///////PING IT HOME END
            
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/undo_import/stage2.php');
            break;
        default:
            require_once (plugin_dir_path(dirname(__FILE__)) . 'admin/templates/undo_import/stage1.php');
            break;
        }
    }

    /**
     *                                  ###                             ###
     *                                  ### METHODS INVOKED VIA AJAX    ###
     *                                  ###                             ###
     */
     
    /**
     *  Import Single Page - Read a remote URL
     *  
     *  @return Base64 encoded version of the HTML retrieved from a given URL
     *  
     *  @details Retrieves content from the URL sent the $_POST vars, then base64 encodes it before returning in JSON string
     */
    public function vwi_ajax_read_url()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
         //   exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        // User Agent
        // converted to CURL
        $options = array(
            CURLOPT_USERAGENT =>get_option('vwi_user_agent','vwiteImporterBot/1.0 (+https://www.vwiteimporter.com)'),
            CURLOPT_POST=>false,
            CURLOPT_CUSTOMREQUEST=>'GET',
        );
        $result = $this->retrieve_file(rtrim($_POST["vwi_url"], '/') , $options);
        if (isset($result['headers']['response_code']) && $result['headers']['response_code'] == 200) {
            if (!(strpos($result['headers']['Content-Type'], 'text/html') === false)) {
                if ($result['content']) {
                    //Character encoding issues now dealt with via fix #171
                    wp_send_json_success(base64_encode($result['content']));
                }
                else {
                    $error = error_get_last();
                    wp_send_json_error($result['errinfo']);
                }
            }
            else {
                wp_send_json_error('Content is not text/html it is ' . $result['headers']['Content-Type']);
            }
        }
        else {
            wp_send_json_error(implode(". ",array($result['headers']['status_path'],$result['errinfo'])));
        }

        wp_die();
    }   
    
    /**
     *  Import Single Page - Extract content from page
     *  
     *  @return Array of page structure including the actual readable content
     *  
     *  @details Makes a web services call to extract meaningful content from HTML
     */
    public function vwi_ajax_extract_content()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        // Set up the data to post to target URL
        $vwi_h1_as_titles=(get_option('vwi_h1_as_titles')=='on');
        $vwi_remove_h1_matching_titles=(get_option('vwi_remove_h1_matching_titles')=='on');
        $data = array(
            'vwi_api_user' => get_option('vwi_api_user') ,
            'vwi_api_key' => get_option('vwi_api_key') ,
            'vwi_url' => rtrim($_POST['vwi_url'], '/') , // The url we got the content from
            'vwi_min_para_size' => $_POST['vwi_min_para_size'],
            'vwi_main_image' => $_POST['vwi_main_image'],
            'vwi_other_images' => $_POST['vwi_other_images'],
            'vwi_h1_as_titles' => $vwi_h1_as_titles,
            'vwi_remove_h1_matching_titles'=>$vwi_remove_h1_matching_titles,
            'vwi_extraction_failure'=>$_POST['vwi_extraction_failure'],
            'vwi_attribute_alt'=>get_option('vwi_attribute_alt','on')=='on',
            'vwi_attribute_align'=>get_option('vwi_attribute_align','on')=='on',
            'vwi_attribute_class'=>get_option('vwi_attribute_class')=='on',
            'vwi_attribute_height'=>get_option('vwi_attribute_height','on')=='on',
            'vwi_attribute_id'=>get_option('vwi_attribute_id')=='on',
            'vwi_attribute_name'=>get_option('vwi_attribute_name')=='on',
            'vwi_attribute_title'=>get_option('vwi_attribute_title','on')=='on',
            'vwi_attribute_width'=>get_option('vwi_attribute_width','on')=='on',            
            'vwi_content' => $_POST['vwi_content'], // Base 64 encoded version of the content
        );

        $post_data = http_build_query($data);

        // Build our post header converted to CURL
        $opts = array(
            CURLOPT_CUSTOMREQUEST=>'POST',
            CURLOPT_POST=>true,
            CURLOPT_POSTFIELDS=>$post_data,
        );

        // Turn that into a stream context @TODO - extend caching for this also???

       // $fetched = $this->file_get_contents_curl($this->service_endpoints['extract_content'], null, $opts);
	  $fetched = $_POST['vwi_content'];
	   $content = $fetched['content'];
        if ($content) {
            $decoded = json_decode($content);
            if(isset($decoded->error)){
                wp_send_json_error($decoded->error);
            }else{
                wp_send_json_success($content);
            }
        }
        else {
            $error = error_get_last();
            wp_send_json_error($error['message']);
        }

        wp_die();
    }   
    
    /**
     *  Import Single Page - Insert Page or Post
     *  
     *  @return Details of the post inserted
     *  
     *  @details Checks we haven't already inserted this content then proceeds to insert into Wordpress
     */
    public function vwi_ajax_insert_page_or_post()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        $page_data = json_decode(stripcslashes($_POST['vwi_page_or_post_data']));
        /*
        ** Did we actually get any content
        */
        if (strpos($page_data->html, '[unable to retrieve full-text content]') && $_POST['vwi_extraction_failure'] == 'skip_page') {
            return wp_send_json_error('Failed to extract page content - skipping page insertion');
        }

        /*
        ** Check we haven't already inserted this content
        */
        if(!strpos($page_data->html, '[unable to retrieve full-text content]')){
            $md5_hash = md5($page_data->html . "||" . $page_data->title);
            $args = array(
                'posts_per_page' => 1,
                'post_type' => array(
                    'post',
                    'page'
                ) ,
                'meta_query' => array(
                    array(
                        'key' => 'vwi_content_hash',
                        'value' => $md5_hash,
                    )
                )
            );
            $post_query_results = new WP_Query($args);
            if ($post_query_results->post_count > 0) {
                return wp_send_json_error("This content has already been imported.");
            }
        }
        // Create post object

        $my_post = array(
            'post_title' => $page_data->title,
            'post_content' => $page_data->html,
            'post_status' => $_POST['vwi_publish'] == 'true' ? 'publish' : 'draft',
            'post_type' => $_POST['vwi_page_or_post_type'],
            'meta_input' => array(
                'vwi_content_hash' => $md5_hash,
                'vwi_source_url' => rtrim($_POST['vwi_url'], '/'),
                'vwi_description' => $page_data->description,
                'vwi_keywords' => $page_data->keywords,
                'vwi_featured_image' => rtrim($page_data->image, '/'),
            ) ,
        );

        /**
         *  Set post date to match source if can be resolved
         */
        if(isset($page_data->date)){
            if(get_option('vwi_import_publish_date','on')=='on'){
                $my_post['post_date']=$page_data->date;
            }
        }
        // Insert the post into the database

        $post_id = wp_insert_post($my_post);
        if ($post_id) {

            // Do we need to set it to the front page?

            if ($_POST['vwi_front_page'] == 'true') {
                update_option('show_on_front', 'page');
                update_option('page_on_front', $post_id);
            }

            $result['post_id'] = $post_id;
            $result['post_title'] = $page_data->title;
            $result['failed_content_extraction'] = strpos($page_data->html, '[unable to retrieve full-text content]') > 0 ? true : false;
            $result['failed_content_substituted'] = false;
            if(isset($page_data->full_page_substitute)){
                $result['failed_content_extraction'] = true;
                $result['failed_content_substituted'] = true;
            }
            wp_send_json_success($result);
        }
        else {
            $error = error_get_last();
            wp_send_json_error($error['message']);
        }

        wp_die();
    }   
    
   /**
     * Import Multiple Pages - Read URL Links
     *
     * Process
     *  - Enter the website's URL
     *  - Set a Scan Depth for how deeply to spider the site
     *  - Set a Parallelisation parameter for how many pages to import simultaneously
     *  - Set a Breathing Space parameter so the routine does not overwhelm the server
     *  - Be able to review the pages found and see any broken links
     *  - Confirm which pages to import and how to import them
     *  - See a review of what took place after import
     *  
     * @return Array of url links
     */
    public function vwi_ajax_read_url_links()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            //exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        // converted to CURL
        $options = array(
            CURLOPT_USERAGENT =>get_option('vwi_user_agent','vwiteImporterBot/1.0 (+https://www.vwiteimporter.com)'),
            CURLOPT_POST=>false,
            CURLOPT_CUSTOMREQUEST=>'GET',
        );
		$net_result = $this->retrieve_file(rtrim($_POST["vwi_url"], '/') , $options);
		
        if (isset($net_result['headers']['response_code']) && $net_result['headers']['response_code'] == 200) {
            if (!(strpos($net_result['headers']['Content-Type'], 'text/html') === false)) {
                if ($net_result['content']) {
                    $result = array();
                    $current_charset=get_option('vwi_charset','-Automatic Detection-');
                    $xt_doc = new DOMDocument("1.0", "UTF-8");
                    libxml_clear_errors();
                    $previous = libxml_use_internal_errors(true);
                    // encoding management, remove html entity encoding
                    //@TODO Understand why we need to convert it back from UTF-8 to original here.
                    $current_charset=get_option('vwi_charset','-Automatic Detection-');
                    if($current_charset=='-None-'){
                        $net_result['content']=mb_convert_encoding($net_result['content'],'UTF-8');
                    }elseif($current_charset=='-Automatic Detection-'){
                        //Do nothing
                    }else{
                        $net_result['content']=iConv('UTF-8',$current_charset,$net_result['content']);
                    }
                    $xt_doc->loadHTML($net_result['content'] , LIBXML_COMPACT || LIBXML_HTML_NOIMPLIED || LIBXML_NOBLANKS || LIBXML_NOEMPTYTAG || LIBXML_NOERROR);
                    libxml_clear_errors();
                    libxml_use_internal_errors($previous);
                    $xt_doc->normalize();
                    /*
                    ** Get the actual page title
                    */
                    $page_title = "{none}";
                    foreach($xt_doc->getElementsByTagName('title') as $title) {
                        $page_title = $title->nodeValue;
                    }

                    $links = array();
                    foreach($xt_doc->getElementsByTagName('a') as $link) {
                        // Filter known media types from spider
                        $target_url=$this->absolute_url($link->getAttribute('href'),$_POST["vwi_url"],true,true);
                        if(!$target_url){
                            continue;
                        }
                        $links[] = array(
                            'url' => $target_url,
                            'text' => $link->nodeValue,
                            'orig' => $link->getAttribute('href')
                        );
                    }
                    //Repeat for iFrames
                    foreach($xt_doc->getElementsByTagName('iframe') as $link) {
                        $target_url=$this->absolute_url($link->getAttribute('src'),$_POST["vwi_url"],true);
                        if(!$target_url){
                            continue;
                        }
                        $links[] = array(
                            'url' => $target_url,
                            'text' => $link->nodeValue,
                            'orig' => $link->getAttribute('src')
                        );
                    }
                    //Repeat for Frames
                    foreach($xt_doc->getElementsByTagName('frame') as $link) {
                        $target_url=$this->absolute_url($link->getAttribute('src'),$_POST["vwi_url"],true);
                        if(!$target_url){
                            continue;
                        }
                        $links[] = array(
                            'url' => $target_url,
                            'text' => $link->nodeValue,
                            'orig' => $link->getAttribute('src')
                        );
                    }
                    $result['page_url'] = rtrim($_POST["vwi_url"], '/');
                    $result['links'] = $links;
                    $result['page_title'] = $page_title;
                    wp_send_json_success($result);
                }
                else {
                    $result['page_url'] = rtrim($_POST["vwi_url"], '/');
                    $result['error'] = 'ERROR: No content found';
                    $result['type'] = 'No Content';
                    //Override the "No Content" msg if we have a better one
                    if($net_result['errinfo']){
                        $result['error'] = $net_result['errinfo'];
                    }
                    wp_send_json_error($result);
                }
            }
            else {
                $result['page_url'] = rtrim($_POST["vwi_url"], '/');
                $result['error'] = 'Content is not text/html it is ' . $net_result['headers']['Content-Type'];
                $result['type'] = 'Content';
                wp_send_json_error($result);
            }
        }
        else {
            $result['page_url'] = rtrim($_POST["vwi_url"], '/');
            $result['error'] = implode(". ",array($net_result['headers']['status_path'],$net_result['errinfo']));
            $result['type'] = 'Status';
            wp_send_json_error($result);
        }

        wp_die();
    }

    /**
     * Import Menus - Extract Menu
     *
     * Trigger web services call to extract menus from provided HTML
     * HTML is in base64 encoded format
     *
     * @return Array of menu structures
     * @since    0.1.0
     */ 
    public function vwi_ajax_extract_menu()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
           // exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        // Set up the data to post to target URL

        $data = array(
            'vwi_api_user' => get_option('vwi_api_user') ,
            'vwi_api_key' => get_option('vwi_api_key') ,
            'vwi_url' => rtrim($_POST['vwi_url'], '/') , // The url we got the content from
            'vwi_menu_density' => $_POST['vwi_menu_density'], // Min menu density we're seeking
            'vwi_menu_size' => $_POST['vwi_menu_size'], // Min menu size we're seeking
            'vwi_content' => $_POST['vwi_content'], // Base 64 encoded version of the content
        );

        // Queryise it

        $post_data = http_build_query($data);

        // Build our post header, converted to CURL
        $opts = array(
            CURLOPT_CUSTOMREQUEST=>'POST',
            CURLOPT_POST=>true,
            CURLOPT_POSTFIELDS=>$post_data,
        );

        // Turn that into a stream context @TODO - extend caching to this also???

        $fetched = $this->file_get_contents_curl($this->service_endpoints['extract_menu'], null, $opts);
        $content = $fetched['content'];
        if ($content) {
            // API Key validation
            $decoded = json_decode($content);
            if(isset($decoded->error)){
                wp_send_json_error($decoded->error);
            }else{
                wp_send_json_success($content);
            }
        }
        else {
            $error = error_get_last();
            wp_send_json_error($error['message']);
        }

        wp_die();
    }
    
    /**
     * Link Menus with Content - Update Menus
     *
     * Iterates menus and attempts to link to imported content
     * When imported, menus have a "Custom Link" to external URL of a page
     * When imported, pages have a "Custom Meta" to external URL of a page
     * Where these two match, the menu can be switched to the page
     *
     * @return Array of summary of updates
     * @since    0.1.0
     */     
    public function vwi_ajax_update_menus()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();
        
        // Get all pages and posts
        $args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $post_query_results = new WP_Query($args);
        $results = array();
        $updated = array();
        $already = array();
        $count = 1;
        if ($post_query_results->posts) {
            foreach($post_query_results->posts as $post) {
                $results[$post->ID]['id'] = $post->ID;
                $results[$post->ID]['post_type'] = $post->post_type;
                $results[$post->ID]['url'] = get_post_meta($post->ID, 'vwi_source_url', true);
            }
        }

        $menu_item_id = 0;
        $nav_menus = wp_get_nav_menus();

        // Iterate each menu item

        foreach($nav_menus as $nav_menu) {
            $nav_menu_items = wp_get_nav_menu_items($nav_menu->term_id);
            foreach($nav_menu_items as $nav_menu_item) {
                if ($nav_menu_item->type == 'custom') {

                    // Iterate every page

                    foreach($results as $key => $page) {
                        $url = $page['url'];
                        $page_or_post_id = $page['id'];
                        $page_or_post_type = $page['post_type'];
                        if ($nav_menu_item->url == $url) {

                            // Link them

                            $menu_item_id = wp_update_nav_menu_item($nav_menu->term_id, $nav_menu_item->db_id, array(
                                'menu-item-object-id' => $page_or_post_id,
                                'menu-item-title' => $nav_menu_item->title,
                                'menu-item-parent-id' => $nav_menu_item->menu_item_parent,
                                'menu-item-object' => strtolower($page_or_post_type) ,
                                'menu-item-status' => 'publish',
                                'menu-item-type' => 'post_type',
                                'menu-item-position' => $nav_menu_item->menu_order,
                            ));
                            $updated[$count]['menu_name'] = $nav_menu->name;
                            $updated[$count]['menu_title'] = $nav_menu_item->title;
                            $count++;
                        }
                    }
                }
                else {
                    $already[$count]['menu_name'] = $nav_menu->name;
                    $already[$count]['menu_title'] = $nav_menu_item->title;
                    $count++;
                }
            }
        }

        $skipped = array();
        $count = 1;
        foreach($nav_menus as $nav_menu) {
            $nav_menu_items = wp_get_nav_menu_items($nav_menu->term_id);
            foreach($nav_menu_items as $nav_menu_item) {
                if ($nav_menu_item->type == 'custom') {
                    $skipped[$count]['menu_name'] = $nav_menu->name;
                    $skipped[$count]['menu_title'] = $nav_menu_item->title;
                    $count++;
                }
            }
        }

        $error = error_get_last();
        if ($error) {
            wp_send_json_error($error['message']);
        }
        else {
            wp_send_json_success(array(
                'updated' => $updated,
                'skipped' => $skipped,
                'already' => $already
            ));
        }

        wp_die();
    }

    /**
     * Update Internal Links - Ajax Update Links
     *
     * Iterates links and updates them
     * When imported, pages have a "Custom Meta" to external URL of a page
     * So, if the href attribute of a link points to such a page it can be rewritten
     *
     * @return Array of summary of updates
     * @since    0.1.0
     */     
    public function vwi_ajax_update_links()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();
        
        // Get all pages and posts
        $args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $post_query_results = new WP_Query($args);
        $results = array();
        $imported_pages = array();
        $links_updated = array();
        $links_skipped = array();
        if ($post_query_results->posts) {
            foreach($post_query_results->posts as $post) {
                $results[$post->ID]['id'] = $post->ID;
                $results[$post->ID]['post_type'] = $post->post_type;
                $results[$post->ID]['url'] = get_post_meta($post->ID, 'vwi_source_url', true);
                $results[$post->ID]['content'] = $post->post_content;
                $results[$post->ID]['permalink'] = get_permalink($post->ID);
                $results[$post->ID]['title'] = $post->post_title;
                $imported_pages[$results[$post->ID]['url']] = $results[$post->ID]['permalink'];
            }
        }

        /*
        ** Iterate each page
        */
        $found_any = false;
        $count = 1;
        foreach($results as $key => $page) {
            /*
            ** Identify all links on page
            */
            $remote_url_parts = parse_url($page['url']);
            unset($remote_url_parts['query']); //Remove any query part for now so it doesn't get added to all the page links
            $doc = new DOMDocument();
            libxml_clear_errors();
            $previous = libxml_use_internal_errors(true);
            // encoding management, remove html entity encoding
            $doc->loadHTML('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $page['content'] . '</html>', LIBXML_COMPACT || LIBXML_HTML_NOIMPLIED || LIBXML_NOBLANKS || LIBXML_NOEMPTYTAG || LIBXML_NOERROR);
            libxml_clear_errors();
            libxml_use_internal_errors($previous);
            $found_one = false;
            foreach($doc->getElementsByTagName('a') as $link) {
                /*
                ** Add scheme and domain to the link if it isn't already there
                */
                $link_url_parts = parse_url($link->getAttribute('href')) + $remote_url_parts;
                /*
                ** Rebuild the full url
                */
                $rebuilt_url = rtrim($link_url_parts['scheme'] . "://" . $link_url_parts['host'] . '/' . ltrim($link_url_parts['path'], '/') , '/');
                if (isset($link_url_parts['query'])) {
                    $rebuilt_url.= '?' . $link_url_parts['query'];
                }

                /*
                ** Is it an internal link?
                */
                if ($link_url_parts['host'] == $remote_url_parts['host']) {
                    /*
                    ** Does link point to a page we've inserted?
                    */
                    if (isset($imported_pages[$rebuilt_url])) {
                        /*
                        ** Change the link to the imported page's permalink
                        */
                        $links_updated[$page['title']][$count]['old_url'] = $rebuilt_url;
                        $links_updated[$page['title']][$count]['new_url'] = $imported_pages[$rebuilt_url];
                        $links_updated[$page['title']][$count]['page_id'] = $page['id'];
                        $link->setAttribute('href', $imported_pages[$rebuilt_url]);
                        $found_one = true;
                        $found_any = true;
                    }
                    else {
                        $links_skipped[$page['title']][$count]['url'] = $rebuilt_url;
                        $links_skipped[$page['title']][$count]['message'] = 'Link is internal but target post/page has not been imported.';
                        $links_skipped[$page['title']][$count]['page_id'] = $page['id'];
                    }
                }
                else {
                    $links_skipped[$page['title']][$count]['url'] = $rebuilt_url;
                    if ($link_url_parts['host'] == $_SERVER['HTTP_HOST']) {
                        $links_skipped[$page['title']][$count]['message'] = 'Link has already been updated.';
                        $links_skipped[$page['title']][$count]['page_id'] = $page['id'];
                    }
                    else {
                        $links_skipped[$page['title']][$count]['message'] = 'Link is external.';
                        $links_skipped[$page['title']][$count]['page_id'] = $page['id'];
                    }
                }

                $count++;
            }

            /*
            ** If we changed anything, update the page/post content with new version
            */
            if ($found_one) {
                wp_update_post(array(
                    'ID' => $page['id'],
                    'post_content' => preg_replace('/^<!DOCTYPE.+?>/', '', str_replace(array(
                        '<html>',
                        '</html>',
                        '<body>',
                        '</body>'
                    ) , array(
                        '',
                        '',
                        '',
                        ''
                    ) , $doc->saveHTML()))
                ));
            }
        }

        if ($found_any) {
            $results = array(
                'updated' => $links_updated,
                'skipped' => $links_skipped
            );
            wp_send_json_success($results);
        }
        else {
            $error = error_get_last();
            if ($error) {
                wp_send_json_error($error['message']);
            }
            else {
                wp_send_json_success(array(
                    'updated' => 'No links were found that needed updating.',
                    'skipped' => $links_skipped
                ));
            }
        }

        wp_die();
    }
    
    /**
     * Localise Images - Ajax Scan for Images
     *
     * Locates all images referenced in the content
     *
     * @return Array of image details
     * @since    0.1.0
     */     
    public function vwi_ajax_scan_for_images()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();
        
        // Get all pages and posts
        $args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $post_query_results = new WP_Query($args);
        $results = array();
        $imported_pages = array();
        $links_updated = array();
        $links_skipped = array();
        if ($post_query_results->posts) {
            foreach($post_query_results->posts as $post) {
                $results[$post->ID]['id'] = $post->ID;
                $results[$post->ID]['post_type'] = $post->post_type;
                $results[$post->ID]['url'] = get_post_meta($post->ID, 'vwi_source_url', true);
                $results[$post->ID]['content'] = $post->post_content;
                $results[$post->ID]['permalink'] = get_permalink($post->ID);
                $results[$post->ID]['title'] = $post->post_title;
                $results[$post->ID]['featured_image'] = get_post_meta($post->ID, 'vwi_featured_image', true);
                $imported_pages[$results[$post->ID]['url']] = $results[$post->ID]['permalink'];
            }
        }

        /*
        ** Iterate each page
        */
        $found_any = false;
        $count = 1;
        foreach($results as $key => $page) {
            /*
            ** Identify all images on page
            */
            $remote_url_parts = parse_url($page['url']);
            unset($remote_url_parts['query']); //Remnove any query part for now so it doesn't get added to all the page links
            $doc = new DOMDocument();
            libxml_clear_errors();
            $previous = libxml_use_internal_errors(true);
            // encoding management, remove html entity encoding
            $doc->loadHTML('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $page['content'] . '</html>', LIBXML_COMPACT || LIBXML_HTML_NOIMPLIED || LIBXML_NOBLANKS || LIBXML_NOEMPTYTAG || LIBXML_NOERROR);
            libxml_clear_errors();
            libxml_use_internal_errors($previous);
            
            /*
            ** Include the featured image if we have one (and it's not already set)
            */
            if($page['featured_image']>''){
                if(!get_post_thumbnail_id( $page['id'] )){
                    $node = $doc->createElement('img'); //If a uncomment this line the script just times out
                    $node->setAttribute('src', $page['featured_image']);
                    foreach($doc->getElementsByTagName('body') as $body){
                        $body->appendChild($node);
                        break;
                    }
                }
            }
            
            /*
            ** Then process all of the images
            */
            foreach($doc->getElementsByTagName('img') as $image) {
                /*
                ** Add scheme and domain to the link if it isn't already there
                */
                $image_url_parts = parse_url($image->getAttribute('src')) + $remote_url_parts;
                $image_title = $image->hasAttribute('title')?$image->getAttribute('title'):'-';
                $image_alt = $image->hasAttribute('alt')?$image->getAttribute('alt'):'-';
                /*
                ** Rebuild the full url
                */
                $rebuilt_url = rtrim($image_url_parts['scheme'] . "://" . $image_url_parts['host'] . '/' . ltrim($image_url_parts['path'], '/') , '/');
                if (isset($image_url_parts['query'])) {
                    $rebuilt_url.= '?' . $image_url_parts['query'];
                }

                /*
                ** Is it an internal link?
                */
                if ($image_url_parts['host'] == $remote_url_parts['host']) {
                    $images['internal'][$rebuilt_url][$count]['page'] = $page['title'];
                    $images['internal'][$rebuilt_url][$count]['title'] = $image_title;
                    $images['internal'][$rebuilt_url][$count]['alt'] = $image_alt;
                    $found_any = true;
                }
                else {
                    $images_skipped[$page['title']][$count]['url'] = $rebuilt_url;
                    if ($image_url_parts['host'] == $_SERVER['HTTP_HOST']) {
                        $images['previously'][$rebuilt_url][$count]['page'] = $page['title'];
                        $images['previously'][$rebuilt_url][$count]['title'] = $image_title;
                        $images['previously'][$rebuilt_url][$count]['alt'] = $image_alt;
                        $found_any = true;
                    }
                    else {
                        $images['external'][$rebuilt_url][$count]['page'] = $page['title'];
                        $images['external'][$rebuilt_url][$count]['title'] = $image_title;
                        $images['external'][$rebuilt_url][$count]['alt'] = $image_alt;
                        $found_any = true;
                    }
                }

                $count++;
            }
        }

        if ($found_any) {
            $results = $images;
            wp_send_json_success($results);
        }
        else {
            $error = error_get_last();
            if ($error) {
                wp_send_json_error($error['message']);
            }
            else {
                wp_send_json_success('No images were found.');
            }
        }

        wp_die();
    }

    /**
     * Localise Images - Ajax Import Images
     *
     * Uploads image to Media Libray
     *
     * @return Details of whether upload worked
     * @since    0.1.0
     */         
    public function vwi_ajax_import_images()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();

        /*
        ** Place image in upload folder
        */
        if (!class_exists('WP_Http')) include_once (ABSPATH . WPINC . '/class-http.php');

        $photo = new WP_Http();
        $photo = $photo->request($_POST['vwi_url']);
        if ($photo['response']['code'] == 200) {
            $url_parts = parse_url($_POST['vwi_url']);
            $attachment = wp_upload_bits(basename($url_parts['path']) , null, $photo['body'], date("Y-m", strtotime($photo['headers']['last-modified'])));
            if (empty($attachment['error'])) {
                /*
                ** Generate a suitable title
                */
                $vwi_title='';
                if($_POST['vwi_title']!='-'){
                    $vwi_title=$_POST['vwi_title'];
                }else{
                    $vwi_title=basename($url_parts['path']);
                }
                $vwi_title=preg_replace( '%\s*[-_\s]+\s*%', ' ',  $vwi_title );
                $vwi_title=ucwords( strtolower( $vwi_title ) );
                $vwi_title=str_replace('.jpg','',$vwi_title);
                $vwi_title=str_replace('.png','',$vwi_title);
                $vwi_title=str_replace('.gif','',$vwi_title);
                $vwi_title=str_replace('.tif','',$vwi_title);
                
                /*
                ** Generate a suitable Alt
                */
                $vwi_alt='';
                if($_POST['vwi_alt']!='-'){
                    $vwi_alt=$_POST['vwi_alt'];
                }else{
                    $vwi_alt=$vwi_title;
                }
                $vwi_alt=preg_replace( '%\s*[-_\s]+\s*%', ' ',  $vwi_alt );
                $vwi_alt=ucwords( strtolower( $vwi_alt ) );
                /*
                ** Import image into the media library
                */
                $filetype = wp_check_filetype(basename($attachment['file']) , null);
                $postinfo = array(
                    'post_mime_type' => $filetype['type'],
                    'post_title' => $vwi_title , 
                    'post_excerpt' => $vwi_alt , 
                    'post_content' => $vwi_alt,
                    'post_status' => 'inherit',
                );
                $filename = $attachment['file'];

                // $attach_id = wp_insert_attachment( $postinfo, $filename, $postid );

                $attach_id = wp_insert_attachment($postinfo, $filename);
                if ($attach_id) {
                    //Update the Alt text
                    update_post_meta( $attach_id, '_wp_attachment_image_alt',$vwi_alt );
                    if (!function_exists('wp_generate_attachment_data')) require_once (ABSPATH . "wp-admin" . '/includes/image.php');

                    $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                    if (wp_update_attachment_metadata($attach_id, $attach_data)) {
                        /*
                        ** Generate the new filename
                        */
                        $new_file = wp_upload_dir();
                        $new_file = $new_file['baseurl'] . '/' . $attach_data['file'];
                        return wp_send_json_success(array(
                            'old_url' => $_POST['vwi_url'],
                            'new_url' => $new_file,
                            'vwi_index' => $_POST['vwi_index'],
                            'attach_id' => $attach_id,
                        ));
                    }
                    else {
                        return wp_send_json_error(array(
                            'error' => 'Attachment data update failed',
                            'vwi_index' => $_POST['vwi_index']
                        ));
                    }
                }
                else {
                    return wp_send_json_error(array(
                        'error' => 'Attachment could not be created',
                        'vwi_index' => $_POST['vwi_index']
                    ));
                }
            }
            else {
                return wp_send_json_error(array(
                    'error' => $attachment['error'],
                    'vwi_index' => $_POST['vwi_index']
                ));
            }
        }
        else {
            return wp_send_json_error(array(
                'error' => 'Image cannot be loaded',
                'vwi_index' => $_POST['vwi_index']
            ));
        }

        wp_die();
    }

    /**
     * Localise Images - Ajax Update Images
     *
     * Updates content references to old images to new media library images
     *
     * @return Array of update details
     * @since    0.1.0
     */         
    public function vwi_ajax_update_images()
    {
        if (!wp_verify_nonce($_POST['data_nonce'], "vwi")) {
            exit("No naughty business please");
        }

        // Clear any pre-existing errors/warnings
        $this->clear_last_error();
        
        $args = array(
            'posts_per_page' => 50,
            'offset'=>$_POST['vwi_offset'],
            'order'=>'ASC',
            'orderby'=>'ID',
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $_POST['vwi_image_list']=json_decode(stripcslashes($_POST['vwi_image_list']),true);
        $post_query_results = new WP_Query($args);
        $found_any = false;
        $updated_links = 0;
        $updated_pages = 0;
        if ($post_query_results->posts) {
            foreach($post_query_results->posts as $post) {
                $doc = new DOMDocument();
                libxml_clear_errors();
                $previous = libxml_use_internal_errors(true);
                // encoding management, remove html entity encoding
                $doc->loadHTML('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $post->post_content . '</html>', LIBXML_COMPACT || LIBXML_HTML_NOIMPLIED || LIBXML_NOBLANKS || LIBXML_NOEMPTYTAG || LIBXML_NOERROR);
                libxml_clear_errors();
                libxml_use_internal_errors($previous);
                $found_one = false;
                $remote_url_parts = parse_url(get_post_meta($post->ID, 'vwi_source_url', true));
                unset($remote_url_parts['query']); //Remnove any query part for now so it doesn't get added to all the page links
                foreach($doc->getElementsByTagName('img') as $image) {
                    $image_url_parts = parse_url($image->getAttribute('src')) + $remote_url_parts;
                    $rebuilt_url = rtrim($image_url_parts['scheme'] . "://" . $image_url_parts['host'] . '/' . ltrim($image_url_parts['path'], '/') , '/');
                    if (isset($image_url_parts['query'])) {
                        $rebuilt_url.= '?' . $image_url_parts['query'];
                    }

                    /*
                    ** Is this one of our images to replace?
                    */
                    foreach($_POST['vwi_image_list'] as $key => $value) {
                        if ($rebuilt_url == $value['old_url']) {
                            $image->setAttribute('src', $value['new_url']);
                            $found_one = true;
                            $found_any = true;
                            $updated_links++;
                            break;
                        }
                    }
                }

                if ($found_one) {
                    wp_update_post(array(
                        'ID' => $post->ID,
                        'post_content' => preg_replace('/^<!DOCTYPE.+?>/', '', str_replace(array(
                            '<html>',
                            '</html>',
                            '<body>',
                            '</body>'
                        ) , array(
                            '',
                            '',
                            '',
                            ''
                        ) , $doc->saveHTML()))
                    ));
                    $updated_pages++;
                }
                
                /**
                 *  Also set the featured image
                 */
                 $featured_image = get_post_meta($post->ID, 'vwi_featured_image', true);
                 if($featured_image>''){
                    foreach($_POST['vwi_image_list'] as $key => $value) {
                        if ($featured_image == $value['old_url']) {
                            set_post_thumbnail( $post->ID, $value['attach_id'] );
                            $updated_pages++;
                            $found_any=true;
                            break;
                        }
                    }
                 }
            }
        }

        if ($found_any) {
            $error = error_get_last();
            if ($error) {
                return wp_send_json_error(array(
                    'error' => $error['message']
                ));
            }
            else {
                return wp_send_json_success(array(
                    'links_updated' => $updated_links,
                    'pages_updated' => $updated_pages,
                    'page_count' => $post_query_results->post_count,
                ));
            }
        }
        else {
            return wp_send_json_success(array(
                'links_updated' => $updated_links,
                'pages_updated' => $updated_pages,
                'page_count' => $post_query_results->post_count,
            )); 
        }
    }   
    
    
    /**
     *                                  ###                         ###
     *                                  ### PHP Controller Methods  ###
     *                                  ###                         ###
     */
    
    /**
     * Import Menus - Create WP Menu Set
     *
     * Iterates the desired menu structure
     * Calls vwi_create_menu on each menu to import

     * @param [in] $form_data    The posted form data including the structure of menus to import
     * @return Array of menu structures inserted
     * @since    0.1.0
     */
    private function vwi_create_wp_menu_set($form_data)
    {
        /*
        ** Determine which menus the user wants to import
        */
        $menus = preg_grep('/^vwi_menu_import_checkbox_.*/', array_keys($form_data));
        $result = array();
        $todo = array();
        foreach($menus as $key => $value) {
            $todo[] = str_replace('vwi_menu_import_checkbox_', '', $value);
        }

        /*
        ** Create each one
        */
        foreach($todo as $key => $value) {
            $result[] = $this->vwi_create_wp_menu($form_data, $value);
        }

        return $result;
    }

    /**
     * Import Menus - Create WP Menu
     *
     * Physically inserts a menu into WP
     *
     * @param [in] $form_data   The posted form data including the structure of menus to import
     * @param [in] $index       The specific menu index to import
     * @return Array of menu structures inserted
     * @since    0.1.0
     */ 
    private function vwi_create_wp_menu($form_data, $index)
    {
        $result = array();
        $menu_name = $form_data["vwi_menu_import_name_" . $index];
        $menu_content = json_decode(stripcslashes($form_data["vwi_menu_import_content_" . $index]) , true);
        $menu_exists = wp_get_nav_menu_object($menu_name);
        if (!$menu_exists) {
            $menu_id = wp_create_nav_menu($menu_name);
            $parent_id[-1] = 0;
            foreach($menu_content as $menu_item) {
                /*
                ** Default menu items array settings
                **
                ** $defaults = array(
                ** 'menu-item-db-id' => $menu_item_db_id,
                ** 'menu-item-object-id' => 0,
                ** 'menu-item-object' => '',
                ** 'menu-item-parent-id' => 0,
                ** 'menu-item-position' => 0,
                ** 'menu-item-type' => 'custom',
                ** 'menu-item-title' => '',
                ** 'menu-item-url' => '',
                ** 'menu-item-description' => '',
                ** 'menu-item-attr-title' => '',
                ** 'menu-item-target' => '',
                ** 'menu-item-classes' => '',
                ** 'menu-item-xfn' => '',
                ** 'menu-item-status' => '',
                ** );
                */
                $menu_item_id = wp_update_nav_menu_item($menu_id, 0, array(
                    'menu-item-title' => __($menu_item['text']) ,
                    'menu-item-parent-id' => $parent_id[$menu_item['path'] - 1],

                    // 'menu-item-classes' => 'home',
                    // 'menu-item-url' => home_url( '/' ),

                    'menu-item-url' => $menu_item['link'],
                    'menu-item-status' => 'publish'
                ));
                $parent_id[$menu_item['path']] = $menu_item_id;
            }

            $result['status'] = 'Success';
            $result['message'] = "A menu with the name <em>$menu_name</em> has been created. <a href='nav-menus.php?action=edit&menu=$menu_id'>View It</a>";
        }
        else {
            $result["status"] = "Error";
            $result["message"] = "A menu with the name $menu_name already exists. <a href='nav-menus.php?action=edit&menu=" . $menu_exists->term_id . "'>View It</a>";
        }

        return $result;
    }   
    
    
    /**
     *                                  ###                 ###
     *                                  ### UTILITY METHODS ###
     *                                  ###                 ###
     */
    
    /**
     *  @brief Retrieve File
     *  
     *  @param [in] $filename   Filepath & name or URL to retrieve
     *  @param [in] $options    Context stream options (e.g. request headers)
     *  @return Return          Array containing response headers and content
     *  
     *  @details Retrieves a file from disk or URL, checking cache if appropriate
     *           If retreiving from cache, add dummy headers to simulate a 200 response.
     */
    private function retrieve_file($filename, $options)
    {
        $errinfo="";
        $cachefile = get_temp_dir() . "/" . md5($filename) . ".vwi";
        $max_cache_age = get_option('vwi_max_cache_age');
        $headers = array();
        if (file_exists($cachefile) && (time() - filemtime($cachefile) < $max_cache_age * 60) && $max_cache_age > 0) {
        
            $content = file_get_contents($cachefile);
            $headers['response_code'] = 200;
            $headers['Content-Type'] = 'text/html';
            
        } else {
        
            $result = $this->retrieve_file_from_socket($filename,$options,true);
            $content=$result['content'];
            $headers=$result['headers'];
            
            if (isset($headers['response_code']) ) {
                if ($headers['response_code'] == 200){
                    if (!(strpos($headers['Content-Type'], 'text/html') === false)) {
                    
                        if($content > ''){

                            // encoding management
                            $current_charset=get_option('vwi_charset','-Automatic Detection-');
                            if($current_charset=='-None-'){
                                //Do Nothing
                            }elseif($current_charset=='-Automatic Detection-'){
                                $content = mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
                            }else{
                                $content=iconv ( $current_charset,'UTF-8', $content );
                            }

                            if($content>''){
                                if ($max_cache_age > 0) {
                                    file_put_contents($cachefile, $content);
                                }
                            }else{
                                $errinfo = "ERROR: Character Set Encoding conversion failed. Please check Plugin Settings.";
                            }
                            
                        }elseif($headers['status_path']=='Callback aborted'){
                            $errinfo="File exceeded maximum download size. Consider raising in Plugin Settings.";
                        }else{
                            $errinfo="ERROR: No content extracted. Consider raising timeout in Plugin Settings.";
                        }                    
                    
                    }
                }elseif($headers['response_code'] > 299 && $headers['response_code'] < 400){
                    $errinfo = "ERROR: Attempted to follow redirect but encountered timeout. Consider raising in Plugin Settings.";
                }
                
            }

            //Normally, a HTTP error is returned in the header status path
            //If not there, place the error message in it's place
            if(!isset($headers['status_path'])||$headers['status_path']==""){
                $error = error_get_last();
                $headers['status_path'] = end(explode(':',$error['message']));
                $errinfo = end(explode(':',$error['message']));
            }

        }

        return array(
            'headers' => $headers,
            'content' => $content,
            'errinfo' => $errinfo
        );
    }

    private function retrieve_file_from_socket($filename, $options,$add_slash_and_retry=false){
        /* 
        ** Set timeout options
        */
        //$default_socket_timeout = ini_get('default_socket_timeout');
        //ini_set('default_socket_timeout',get_option('vwi_timeout',15));
        //if(isset($options['http'])){
        //    $options['http']['timeout'] = get_option('vwi_timeout',15);
        //}
        $options[CURLOPT_CONNECTTIMEOUT]=get_option('vwi_timeout',15);
        $options[CURLOPT_TIMEOUT]=get_option('vwi_timeout',15);
        
        /*
        ** Spin up a context and get file
        */
        $fetched = $this->file_get_contents_curl($filename, false, $options);
        $content = $fetched['content'];
        $header = $fetched['header'];

        /*
        ** Handle the response
        */
        //if(isset($http_response_header)){
        //    $headers = $this->parse_headers($http_response_header);
        //}else{
        //    $headers = array();
        //}
        
        if($content === false ){
                    
            if(isset($header['response_code']) && $header['response_code']==='404' && $add_slash_and_retry){
                $result = $this->retrieve_file_from_socket($filename."/", $options,false); //Try again with trailing slash
                $content=$result['content'];
                $header = $result['header'];
            }
            
        }
        
        /* 
        ** Reset timeout options
        */
        //ini_set('default_socket_timeout',$default_socket_timeout);
        return array(
            'headers'=>$header,
            'content'=>$content
        );
    }
    
    /**
     *  @brief   file_get_contents_curl
     *  @param   [in] $url: The URL to retrieve
     *  @param   [in] $use_include_path: n/a
     *  @param   [in] $opts: Options to pass Curl
     *  @return  Array of content and headers
     */
    private function file_get_contents_curl($url, $use_include_path, $opts){
		$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"POST",        //set request type post or get
            CURLOPT_POST           =>true,        //set to GET
			//CURLOPT_POSTFIELDS     =>$opts['http']['content'],
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_FAILONERROR    => true,
            CURLOPT_SSL_VERIFYPEER => false,    // disable SSL certificate verification
            CURLOPT_NOPROGRESS     => false,    // enable progress monitoring
            CURLOPT_PROGRESSFUNCTION => array($this,'curlProgressCallback'),    //progress callback routine
			
        );
        $options = array_replace($options,$opts);
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $http_response_header  = curl_getinfo( $ch );
        curl_close( $ch );
        if($content==''){
            $content=false;
        }
        $http_response_header['status_path']=$errmsg;
        $http_response_header['response_code']=$http_response_header['http_code'];
        $http_response_header['Content-Type']=$http_response_header['content_type'];
		return array('content'=>$content,'header'=>$http_response_header);
	}
    
    /**
     *  @brief   Curl Progress Callback
     *  @param   [in] $resource: The Curl Handle
     *  @param   [in] $download_size: How big the download is (if known)
     *  @param   [in] $downloaded: How much has been downloaded
     *  @param   [in] $upload_size: How big the upload is
     *  @param   [in] $uploaded: How much has been uploaded
     *  @return  Return non-zero to abort
     */
    function curlProgressCallback($resource, $download_size = 0, $downloaded = 0, $upload_size = 0, $uploaded = 0)
    {
       /**
        * $resource parameter was added in version 5.5.0 breaking backwards compatibility;
        * if we are using PHP version lower than 5.5.0, we need to shift the arguments
        * @see http://php.net/manual/en/function.curl-setopt.php#refsect1-function.curl-setopt-changelog
        */
        if (version_compare(PHP_VERSION, '5.5.0') < 0) {
            $uploaded = $upload_size;
            $upload_size = $downloaded;
            $downloaded = $download_size;
            $download_size = $resource;
        }

        // Handle progress
        if($downloaded > $this->max_download || $download_size > $this->max_download){
            return 1;
        }else{
            return 0;
        }
    }
    
    /**
     *  @brief Parse Headers
     *  
     *  @param [in] $headers An array of headers to be parsed
     *  @return A sanitised version of the headers
     *  
     *  @details Parses response headers, in particular to ensure that if 301 redirects are followed, 
     *           the final 200 status is returned.
     */
    private function parse_headers($headers)
    {
        $head = array();
        if(is_array($headers)){
            foreach($headers as $k => $v) {
                $t = explode(':', $v, 2);
                if (isset($t[1])) $head[trim($t[0]) ] = trim($t[1]);
                else {
                    $head[] = $v;
                    if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) $head['response_code'] = intval($out[1]);
                }
            }
        }

        $counter = 0;
        $status_path = '';
        while (isset($head[$counter])) {
            $status_path.= $head[$counter] . ', ';
            $counter++;
        }

        $status_path = rtrim($status_path, ', ');
        $head['status_path'] = $status_path;
        return $head;
    }
    
    /**
     *  @brief Persist Settings
     *  
     *  @param [in] $settings An array of settings in the form 'setting'=>'value'
     *  @return Return true if success
     *  
     *  @details Iterates the array of settings passed and persists them
     */
    private function persist_settings($settings=array())
    {
        foreach($settings as $setting=>$value){
            update_option($setting, trim($value));            
        }
        return true;
    }
    
    /**
     *  @brief   Brief Description
     *  @param   [in] $url: The URL to process
     *  @param   [in] $example_url: An example URL from same site
     *  @param   [in] $internal_only: Reject external links
     *  @param   [in] $exclude_common_media: Reject links to media type files
     *  @return  An absolute URL or null
     *  @details More details
     */
    private function absolute_url($url,$example_url,$internal_only=true,$exclude_common_media=false){
        //Load valid file extentsions
        require(plugin_dir_path(dirname(__FILE__)) . 'admin/includes/wp-website-importer-extensions.php');

        //First extract a true path from our example URL
        $example_parts=parse_url($example_url);
        if(isset($example_parts['path'])){
            $pathinfo=pathinfo($example_parts['path']);
            if(isset($pathinfo['extension'])){
                //Ticket #187 - Remove known media types
                if(isset($valid_file_extensions[strtolower($pathinfo['extension'])])){
                    $example_parts['path']=$pathinfo['dirname'];
                    if( $example_parts['path'] == '\\'){
                        $example_parts['path']='';
                    }
                }
            }
        }

        /**WBM**/
        // Handle WBM better
        if($example_parts['host']=='web.archive.org'){
            $example_parts['host']=$example_parts['host'].$example_parts['path'];
            $example_parts['path']="";
        }
        
        /*
        ** Rules
        *  If given url starts with a "/" then it is relative to the host (keep this path)
        *  If the given url does not start with a slash, then we need to append it to the given url's path
        *  BUT some sites are special cases - such as The Internet Archive
        */
        $url=trim($url);
        $url_parts=parse_url($url);
        
        //Exit if we have no parse results
        if(!is_array($url_parts)){
            return false;
        }
     
        $href_parts=$url_parts+$example_parts;
        if(isset($href_parts['path'])){
            if($href_parts['path'][0]!="/"){
                // Prevent undefined index
                if(!isset($example_parts['path'])){
                    $example_parts['path']="";
                }
                $href_parts['path']=$example_parts['path'].'/'.$href_parts['path'];
            }else{
                //Do nothing
            }
        }else{
            $href_parts['path'] = '';
        }
        //Remove unnecessary relative links
        $href_parts['path']=ltrim($href_parts['path'],'./');
        
        //See https://stackoverflow.com/questions/4444475/transform-relative-path-into-absolute-url-using-php
        $href_parts['path'] = $this->nodots($href_parts['path']);
        
        //If the link is external, potentially reject it
        if (preg_replace('#^www\.(.+\.)#i', '$1', $href_parts['host']) != preg_replace('#^www\.(.+\.)#i', '$1', $example_parts['host'])) {
            if($internal_only){
                return false;
            }
        }
        
        //If the link points to a common media extension, potentially reject it
        if($exclude_common_media){
            $target_pathinfo=pathinfo($href_parts['path']);
            if(isset($target_pathinfo['extension'])){
                if(isset($common_media_extensions[strtolower($target_pathinfo['extension'])])){
                    return false;
                }
            }
        } 
        
        //If not http or https, reject it
        if ($href_parts["scheme"] != "http" && $href_parts["scheme"] != "https") {
            return false;
        }
        
        $target_url = rtrim($href_parts['scheme'] . "://" . $href_parts['host'] . '/' . ltrim($href_parts['path'], '/') , '/');
        if (isset($href_parts['query']) && $href_parts['query']) {
            $target_url.= '?' . $href_parts['query'];
        }
        
        //Replace common flawed chars
        $target_url=str_replace(" ","%20",$target_url);
        
        //file_put_contents("c:/temp/vwi_log.txt","orig:$url exmpl:$example_url result:$target_url".PHP_EOL,FILE_APPEND);
        return($target_url);
    }
    
    /**
     *  @brief   nodots
     *  @param   [in] $path: The path to process
     *  @return  Processed path
     *  @details Converts relative paths to absolute paths
     */
    function nodots($path) { //Resolve dot dot slashes, no regex!
     $arr1 = explode('/',$path);
     $arr2 = array();
     foreach($arr1 as $seg) {
      switch($seg) {
       case '.':
        break;
       case '..':
        array_pop($arr2);
        break;
       case '...':
        array_pop($arr2); array_pop($arr2);
        break;
       case '....':
        array_pop($arr2); array_pop($arr2); array_pop($arr2);
        break;
       case '.....':
        array_pop($arr2); array_pop($arr2); array_pop($arr2); array_pop($arr2);
        break;
       default:
        $arr2[] = $seg;
      }
     }
     return implode('/',$arr2);
    }
    /**
     *  @brief Remove Images
     *  
     *  @details Iterates all the vwi pages, identifies all images and if they are local, deletes them
     */
    private function vwi_remove_images(){
        $counter=0;
        //Get a list of all images in the media library
        $query_images_args = array(
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'post_status'    => 'inherit',
            'posts_per_page' => - 1,
        );
        $query_images = new WP_Query( $query_images_args );
        $images = array();
        if($query_images->posts){
            foreach ( $query_images->posts as $image ) {
                $images[wp_get_attachment_url($image->ID)]['ID']=$image->ID;
                $images[wp_get_attachment_url($image->ID)]['removed']=false;
            }
        }
        //Get a list of all vwi pages
        $query_post_args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $query_posts = new WP_Query($query_post_args);
        if ($query_posts->posts) {
            foreach($query_posts->posts as $post) {
                $doc = new DOMDocument();
                libxml_clear_errors();
                $previous = libxml_use_internal_errors(true);
                // encoding management, remove html entity encoding
                $doc->loadHTML('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $post->post_content . '</html>', LIBXML_COMPACT || LIBXML_HTML_NOIMPLIED || LIBXML_NOBLANKS || LIBXML_NOEMPTYTAG || LIBXML_NOERROR);
                libxml_clear_errors();
                libxml_use_internal_errors($previous);
                foreach($doc->getElementsByTagName('img') as $image) {
                    $src=$image->getAttribute('src');
                    if(isset($images[$src])){
                        if($images[$src]['removed']){
                            //Do nothing
                        }else{
                            wp_delete_attachment( $images[$src]['ID'] );
                            $images[$src]['removed']=true;
                            $counter++;
                        }
                    }else{
                        //Do nothing
                    }
                }
            }
        }
        return $counter;
    }
    
    /**
     *  @brief Remove Menus
     *  
     *  @return Number of menus removed
     *  @details Iterates all the vwi pages, identifies any menus referencing them, deletes them
     */
    private function vwi_remove_menus(){
        $counter=0;
        $candidate_custom_links=array();
        $candidate_page_links=array();
        
        //Get all importation references
        $query_post_args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $query_posts = new WP_Query($query_post_args);
        if ($query_posts->posts) {
            foreach($query_posts->posts as $post) {
                $candidate_page_links[$post->ID]=true;
                $candidate_custom_links[get_post_meta($post->ID, 'vwi_source_url', true)]=true;
            }
        }

        
        //wp_delete_nav_menu( int|string|WP_Term $menu )
        $menus=wp_get_nav_menus();
        foreach($menus as $menu){
            $menu_items=wp_get_nav_menu_items($menu->term_id);
            
            foreach($menu_items as $menu_item){
                if($menu_item->type=='custom'){
                    if(isset($candidate_custom_links[$menu_item->url])){
                        wp_delete_nav_menu($menu->term_id);
                        $counter++;
                        break;
                    }
                }
                if($menu_item->type=='post_type'){
                    if(isset($candidate_page_links[$menu_item->object_id])){
                        wp_delete_nav_menu($menu->term_id);
                        $counter++;
                        break;
                    }
                }
            }
        }
        return $counter;
    }
    
    /**
     *  @brief Remove Pages
     *  
     *  @return Number of pages removed
     *  @details Iterates all the vwi pages, deletes them
     */
    private function vwi_remove_pages(){
        $counter=0;
        
        //Get all importation references
        $query_post_args = array(
            'posts_per_page' => - 1,
            'post_type' => array(
                'post',
                'page'
            ) ,
            'meta_key' => 'vwi_source_url',
        );
        $query_posts = new WP_Query($query_post_args);
        if ($query_posts->posts) {
            foreach($query_posts->posts as $post) {
                wp_delete_post( $post->ID);
                $counter++;
            }
        }
        return $counter;
    }
    
    /**
     *  @brief Updated Version Check
     *  
     *  @return N/A
     *  @details Checks for an updated version of the plug-in
     */
    public function vwi_updated_version_check(){
        // set auto-update params
        $plugin_current_version = $this->version;
        $plugin_remote_path     = $this->service_endpoints['update_check'];
        $plugin_slug            = "vwi/wp-website-importer.php"; //plugin_basename(__FILE__);
        $license_user           = get_option('vwi_api_user');
        $license_key            = get_option('vwi_api_key');

        // only perform Auto-Update call if a license_user and license_key is given
        if ( $license_user && $license_key && $plugin_remote_path )
        {
            new Wp_Website_Importer_Updater ($plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key);
        }    
    }
    
    /*
    ** Ensure error clear down is pre v7 compatible
    */
    private function clear_last_error(){
        if(function_exists("error_clear_last")){
            error_clear_last();
        }else{
            @trigger_error("");
        }
    }

}

<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://vindiweb.com
 * @since      1.0.0
 *
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 * @author     vince <vince@vindiweb.co.nz>
 */
class Vindi_Web_Importer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Vindi_Web_Importer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'VINDI_WEB_IMPORTER_VERSION' ) ) {
			$this->version = VINDI_WEB_IMPORTER_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'vindi-web-importer';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Vindi_Web_Importer_Loader. Orchestrates the hooks of the plugin.
	 * - Vindi_Web_Importer_i18n. Defines internationalization functionality.
	 * - Vindi_Web_Importer_Admin. Defines all hooks for the admin area.
	 * - Vindi_Web_Importer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-vindi-web-importer-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-vindi-web-importer-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-vindi-web-importer-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-vindi-web-importer-public.php';

		$this->loader = new Vindi_Web_Importer_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Vindi_Web_Importer_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Vindi_Web_Importer_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Vindi_Web_Importer_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		/* Custom hooks */
		$this->loader->add_action( 'admin_menu', $plugin_admin ,'vwi_admin_menu');
		//$this->loader->add_action( 'admin_enqueue_scripts',$plugin_admin , 'vwi_cache_file_cleanup' );
        $this->loader->add_action( 'plugins_loaded',$plugin_admin , 'vwi_updated_version_check' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_read_url',$plugin_admin , 'vwi_ajax_read_url' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_read_url_links',$plugin_admin , 'vwi_ajax_read_url_links' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_extract_menu',$plugin_admin , 'vwi_ajax_extract_menu' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_extract_content',$plugin_admin , 'vwi_ajax_extract_content' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_insert_page_or_post',$plugin_admin , 'vwi_ajax_insert_page_or_post');
		$this->loader->add_action( 'wp_ajax_vwi_ajax_update_menus',$plugin_admin , 'vwi_ajax_update_menus' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_update_links',$plugin_admin , 'vwi_ajax_update_links' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_scan_for_images',$plugin_admin , 'vwi_ajax_scan_for_images' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_import_images',$plugin_admin , 'vwi_ajax_import_images' );
		$this->loader->add_action( 'wp_ajax_vwi_ajax_update_images',$plugin_admin , 'vwi_ajax_update_images' );


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Vindi_Web_Importer_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Vindi_Web_Importer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}

<?php

/**
 * Fired during plugin activation
 *
 * @link       https://vindiweb.com
 * @since      1.0.0
 *
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 * @author     vince <vince@vindiweb.co.nz>
 */
class Vindi_Web_Importer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

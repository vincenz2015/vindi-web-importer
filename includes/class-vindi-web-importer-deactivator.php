<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://vindiweb.com
 * @since      1.0.0
 *
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Vindi_Web_Importer
 * @subpackage Vindi_Web_Importer/includes
 * @author     vince <vince@vindiweb.co.nz>
 */
class Vindi_Web_Importer_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
